﻿using App.WebUI.Services;
using FluentValidation.Results;
using System.Collections.Generic;
using Xunit;

namespace App.Tests.Services
{
    public class ValidatorErrorConverterTest
    {
        [Fact]
        public void CreatesTree()
        {
            var converter = new ValidatorErrorConverter();
            var originErrors = new List<ValidationFailure>();
            originErrors.Add(new ValidationFailure("prop1", "error1"));
            originErrors.Add(new ValidationFailure("prop2", "error2"));

            var result = new Dictionary<string, string>();
            result.Add("prop1", "error1");
            result.Add("prop2", "error2");

            Assert.Equal(converter.Convert(originErrors), result);
        }
    }
}