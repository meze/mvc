﻿using NLog;
using AppLogger = App.Core.Logging;

namespace App.Tests
{
    public static class Utils
    {
        public static AppLogger.Logger GetLogger()
        {
            var logger = LogManager.GetLogger("UnitTestLogger");

            return new AppLogger.Logger(new AppLogger.LoggerConfiguration("tests"), logger);
        }
    }
}