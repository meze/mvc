﻿using App.Core.Logging;
using NLog.Targets;
using System.IO;
using System.Linq;
using Xunit;

namespace App.Tests.Logging
{
    public class LoggerTest
    {
        [Fact]
        public void LogsEvent()
        {
            var logger = new Logger(new LoggerConfiguration(Directory.GetCurrentDirectory() + "/Logging/sample.config.xml"), NLog.LogManager.GetLogger("Test"));
            logger.Log(EventTypes.Unspecified, "Test");

            var target = (MemoryTarget)NLog.LogManager.Configuration.FindTargetByName("memory");

            Assert.Contains("UNSPECIFIED: Test", target.Logs.Last());
        }
    }
}