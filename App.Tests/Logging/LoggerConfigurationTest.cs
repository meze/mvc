﻿using App.Core.Logging;
using NLog.Targets;
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace App.Tests.Logging
{
    public class LoggerConfigurationTest
    {
        [Fact]
        public void readsSeverity()
        {
            var config = new LoggerConfiguration(Directory.GetCurrentDirectory() + "\\Logging\\sample.config.xml");

            Assert.Equal("info", config.GetSeverity("Unspecified"));
            Assert.Equal("debug", config.GetSeverity("ItemCreated"));
            Assert.Null(config.GetSeverity("ItemThatDoesNotExist"));
        }

        [Fact]
        public void throwsExceptionIfNoConfigurationExist()
        {
            Assert.Throws<ArgumentException>(() => new LoggerConfiguration("doesnotexist.config.xml"));
        }
    }
}