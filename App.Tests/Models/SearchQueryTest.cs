﻿using App.Domain.Abstract;
using System;
using Xunit;

namespace App.Tests.Models
{
    internal class StubSearchQuery : SearchQuery
    {
    }

    public class SearchQueryTest
    {
        [Theory]
        [InlineData(10, 1, 0)]
        [InlineData(10, 2, 10)]
        [InlineData(10, 4, 30)]
        public void ShouldCalculateTheNumberOfItemsToSkip(int perPage, int page, int skipNumber)
        {
            var query = new StubSearchQuery
            {
                PerPage = perPage,
                Page = page
            };

            Assert.Equal(skipNumber, query.SkipNumber);
        }

        [Fact]
        public void ShouldIgnoreNegativeValues()
        {
            var query = new StubSearchQuery
            {
                PerPage = -4,
                Page = -3
            };

            Assert.Equal(SearchQuery.DEFAULT_PER_PAGE, query.PerPage);
            Assert.Equal(1, query.Page);
        }

        [Fact]
        public void ShouldNotLetToSetPerPageGreaterThan100()
        {
            var query = new StubSearchQuery
            {
                PerPage = 1000
            };

            Assert.Equal(100, query.PerPage);
        }
    }
}