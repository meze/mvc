﻿using App.Core.Cqrs;
using App.Core.Cqrs.Concrete;
using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace App.Tests.Cqrs
{
    internal class StubCommand : ICommand
    {
        private bool _isExecuted = false;

        public bool IsExecuted()
        {
            return _isExecuted;
        }

        public void Executed()
        {
            _isExecuted = true;
        }
    }

    internal class StubCommandHandler : ICommandHandler<StubCommand>
    {
        public void Execute(StubCommand command)
        {
            command.Executed();
        }
    }

    public class CommandDispatcherTest
    {
        [Fact]
        public void DispatchesACommand()
        {
            var dispatcher = new CommandDispatcher(new StubHandlerFactory());
            var command = new StubCommand();
            dispatcher.Dispatch<StubCommand>(command);

            Assert.True(command.IsExecuted());
        }
    }
}