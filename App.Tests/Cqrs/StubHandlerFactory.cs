﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Cqrs;

namespace App.Tests.Cqrs
{
    class StubHandlerFactory : IHandlerFactory
    {
        IDictionary<Type, object> _dict = new Dictionary<Type, object>();

        public StubHandlerFactory()
        {
            _dict.Add(typeof(ICommandHandler<StubCommand>), new StubCommandHandler());
            _dict.Add(typeof(IQueryHandler<StubQuery, string>), new StubQueryHandler());
        }

        public T Get<T>()
        {
            return (T)_dict[typeof(T)];
        }
    }
}
