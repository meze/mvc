﻿using App.Core.Cqrs;
using App.Core.Cqrs.Concrete;
using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace App.Tests.Cqrs
{
    internal class StubQuery : IQuery
    {
        public string Content = "OK";
    }

    internal class StubQueryHandler : IQueryHandler<StubQuery, string>
    {
        public string Retrieve(StubQuery query)
        {
            return query.Content;
        }
    }

    public class QueryDispatcherTest
    {
        [Fact]
        public void DispatchesACommand()
        {
            var dispatcher = new QueryDispatcher(new StubHandlerFactory(), Utils.GetLogger());
            var query = new StubQuery();
            var result = dispatcher.Dispatch<StubQuery, string>(query);

            Assert.Equal(query.Content, result);
        }
    }
}