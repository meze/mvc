﻿using FluentValidation.Results;
using System.Collections.Generic;

namespace App.WebUI.Services
{
    public class ValidatorErrorConverter
    {
        public IDictionary<string, string> Convert(IList<ValidationFailure> errors)
        {
            var result = new Dictionary<string, string>();

            foreach (var error in errors)
            {
                result.Add(error.PropertyName, error.ErrorMessage);
            }

            return result;
        }
    }
}