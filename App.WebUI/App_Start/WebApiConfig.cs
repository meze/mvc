﻿using App.WebUI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace App.WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new LowerCaseContractResolver();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "AdminApi",
                routeTemplate: ConfigurationManager.AppSettings.Get("ApiPath") + "/Admin/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, area = "Admin", namespaces = new[] { "App.WebUI.Areas.Admin.Controllers.Api" } }
            );
        }
    }
}