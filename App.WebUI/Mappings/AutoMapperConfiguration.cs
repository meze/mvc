﻿using App.Domain.Entities.Structure;
using App.Domain.Models;
using App.WebUI.Areas.Admin.Models;
using AutoMapper;

namespace App.WebUI.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainMappingProfile>();
            });
        }
    }

    public class DomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Organization, OrganizationViewModel>();
            Mapper.CreateMap<PagedResult<Organization>, PagedResult<OrganizationViewModel>>();
            Mapper.CreateMap<Site, SiteViewModel>();
            Mapper.CreateMap<PagedResult<Site>, PagedResult<SiteViewModel>>();
        }
    }
}