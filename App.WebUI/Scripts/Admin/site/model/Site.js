﻿Ext.define('App.site.model.Site', {
    extend: 'App.model.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'title' },
        { name: 'domain' },
        { name: 'basePath' },
        { name: 'ownerEmail' }
    ],

    proxy: {
        url:    '/Api/Admin/Organization/{organizationId}/Site',
        type:   'api',
        reader: {
            rootProperty: function (raw) {
                return raw.hasOwnProperty('items') && raw.hasOwnProperty('total') ? raw.items : raw;
            }
        }
    },

    getTitle: function () {
        return this.get('title');
    }
});



