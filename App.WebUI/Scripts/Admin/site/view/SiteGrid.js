﻿Ext.define('App.site.view.SiteGrid', {
    controller: {
        xclass: 'App.site.controller.SiteController'
    },

    viewModel: {
        xclass: 'App.site.view.model.SiteModel'
    },

    extend:    'App.base.view.Grid',
    xtype:     'site-grid',
    cls:       'site-grid',
    reference: 'siteGrid',

    bind: '{items}',

    setItemId: function (id) {
        console.log('new id:' , id);
    },

    getColumns: function () {
        return [
            {
                text:      'Title',
                width:     300,
                dataIndex: 'title'
            },
            {
                text:      'Domain',
                width:     250,
                dataIndex: 'domain'
            }
        ];
    }
});
