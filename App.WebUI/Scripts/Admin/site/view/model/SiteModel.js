Ext.define('App.site.view.model.SiteModel', {
    extend: 'Ext.app.ViewModel',

    data: {
        item: null
    },

    formulas: {
        modifyTitle: function (get) {
            var item = get('item'),
                result = item && item.phantom ? 'Creating' : 'Modifying',
                title = get('item.title');

            if (item && typeof title === 'string' && title.length > 0) {
                result += ' ' + title;
            }

            return result;
        },
        selected:     function (get) {
            return get('siteGrid.selection');
        }
    },

    stores: {
        items: {
            model: 'App.site.model.Site'
        }
    }
});
