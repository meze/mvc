﻿Ext.define('App.site.view.SiteForm', {
    extend:        'Ext.form.Panel',
    border:        false,
    baseCls:       'x-plain',
    fieldDefaults: {
        labelAlign: 'right',
        layout:     'anchor',
        anchor:     '100%',
        labelWidth: 90
    },
    paramOrder:    ['id'],
    defaultType:   'textfield',
    bodyPadding:   5,

    xtype: 'site-form',
    cls:   'site-form',

    config: {
        activeRecord: null
    },

    items: [{
        xtype:      'hidden',
        bind:       '{item.id}',
        name:       'id',
        rawToValue: function (val) {
            return parseInt(val, 10);
        }
    }, {
        fieldLabel:          'Title',
        name:                'title',
        bind:                '{item.title}',
        allowBlank:          false,
        allowOnlyWhitespace: false
    }, {
        fieldLabel:          'Domain',
        name:                'domain',
        bind:                '{item.domain}',
        allowBlank:          false,
        allowOnlyWhitespace: false
    }, {
        fieldLabel: 'Owner e-mail',
        name:       'ownerEmail',
        vtype:      'email',
        bind:       '{item.ownerEmail}',
        anchor:     '100%'
    }]
});
