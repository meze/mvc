﻿Ext.define('App.site.store.Site', {
    extend:       'Ext.data.Store',
    model:        'App.site.model.Site',
    pageSize:     10,
    remoteFilter: true
});
