Ext.define('App.site.controller.SiteController', {
    extend: 'App.base.controller.GridController',

    add: function () {
        var view = this.getView(),
            site = this.createModel({
                id: null
            }),
            win;

        if (win = this.lookupReference('site-add-window')) {
            win.focus();

            return;
        }

        win = Ext.create('App.base.view.Window', {
            cls:       'site-add-window',
            reference: 'site-add-window',
            items:     [{
                xtype: 'site-form'
            }],
            listeners: {
                createItem: function (item) {
                    view.fireEvent('createItem');
                }
            },
            viewModel: {
                xclass: 'App.site.view.model.SiteModel',
                parent: this.getViewModel(),
                data:   {
                    item: site
                }
            }
        });

        view.add(win);
        win.show();
    },

    onShow: function (params) {
        this.requestParams.organizationId = params.id;
        this.getStore('items').proxy.extraParams = {
            organizationId: params.id
        };

        return this.callParent(arguments);
    },

    editItem: function (record) {
        var view = this.getView(),
            form = Ext.create('App.site.view.SiteForm', {
                activeRecord:     record,
                trackResetOnLoad: true
            }),
            me = this,
            win = Ext.create('App.base.view.Window', {
                cls:       'site-edit-window-' + record.id,
                items:     [form],
                listeners: {
                    changeItem: function (changedData) {
                        view.fireEvent('changeItem', changedData)
                    }
                },
                viewModel: {
                    xclass: 'App.site.view.model.SiteModel',
                    parent: me.getViewModel()
                },
                data:      {
                    item: null
                }
            });

        view.add(win);
        win.show();
        win.body.mask('Loading...');

        this.getModel().getOneById(record.id)
            .then(function (record) {
                win.getViewModel().set('item', me.createModel(record.data));
            })
            .always(function () {
                win.body.unmask();
            })
        ;
    }
});
