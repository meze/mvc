﻿Ext.define('AppLayout', {
    extend:     'Ext.panel.Panel',
    xtype:      'app-layout',
    requires:   [
        'Ext.layout.container.Border'
    ],
    layout:     'border',
    bodyBorder: false,
    defaults:   {
        collapsible: true,
        split:       true
    },

    dockedItems: [{
        xtype: 'toolbar',
        cls: 'app-header',
        dock: 'top',
        items:  [
            {
                xtype: 'tbtext',
                cls: 'logo',
                html: 'MobiTree'
            },
            '->',
            {
                xtype: 'tbtext',
                html: 'You are logged as admin'
            }
        ]
    }],


    initComponent: function () {
        var me = this,
            menu = Ext.create('App.view.MenuPanel');
        Ext.apply(me, {
            items: [menu, {
                collapsible: false,
                region:      'center',
                xtype:       'main',
                menu:        menu
            }]
        });
        me.callParent(arguments);
    }
});
