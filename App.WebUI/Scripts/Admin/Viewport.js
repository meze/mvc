﻿Ext.define('App.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'fit',
    items:  [{
        xtype: 'app-layout'
    }]
});
