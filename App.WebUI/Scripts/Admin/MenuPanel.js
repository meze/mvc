﻿Ext.define('App.view.MenuPanel', {
    extend:      'Ext.tree.Panel',
    xtype:       'menu-panel',
    region:      'west',
    floatable:   false,
    title:       'Navigation',
    width:       225,
    minWidth:    225,
    height:      150,
    rootVisible: false,
    cls:         'main-menu',
    reference:   'main-menu',

    listeners: {
        treeChange: function () {
            this.reloadStore();
        }
    },
    tbar:      [{
        xtype:   'button',
        iconCls: 'x-icon-refresh',
        handler: function () {
            this.up('treepanel').reloadStore();
        }
    }, '->', {
        region: 'center',
        itemId:              'navigation-filter',
        xtype:               'textfield',
        triggers:            {
            clear: {
                cls:     'x-form-clear-trigger',
                handler: 'onClearTriggerClick',
                scope:   'this'
            }
        },
        onClearTriggerClick: function () {
            var store = this.up('treepanel').store;
            this.reset();
            store.clearFilter();
            this.focus();
        },
        listeners:           {
            change: function () {
                var tree = this.up('treepanel'),
                    v;

                try {
                    v = new RegExp(this.getValue(), 'i');
                    tree.store.filter({
                        filterFn: function (node) {
                            var children = node.childNodes,
                                len = children && children.length,
                                visible = v.test(node.get('text')),
                                i,
                                parent;

                            if (node.get('text') === 'Root') {
                                return true;
                            }

                            if (node.get('leaf') !== true) {
                                for (i = 0; i < len; i++) {
                                    if (visible = children[i].get('visible')) {
                                        break;
                                    }
                                }
                            }

                            parent = node.parentNode;
                            while (!visible && parent) {
                                visible = v.test(parent.get('text'));
                                parent = parent.parentNode;
                            }

                            return visible;
                        },
                        id:       'titleFilter'
                    });
                } catch (e) {
                    this.markInvalid('Invalid regular expression');
                }
            },
            buffer: 250
        }
    }],

    initComponent: function () {
        var store = Ext.create('store.Menus');
        Ext.apply(this, {
            store: store
        });
        this.callParent(arguments);

        store.on('load', function () {
            var token = Ext.util.History.getToken();
            var record;
            if (!token) {
                return;
            }
            record = this.getRootNode().findChild('url', token, true);
            if (record) {
                this.getRootNode().expand(true);
                this.expandNode(record, true);
                this.getSelectionModel().select(record);
            }
        }, this);
    },

    reloadStore: function () {
        var store = this.store,
            filter = this.down('#navigation-filter'),
            oldFilterValue = filter.getValue();

        if (store.isLoading()) {
            return false;
        }

        filter.reset();
        store.clearFilter();
        store.reload();
        filter.setValue(oldFilterValue);
    }
});
