﻿Ext.define('App.controller.Admin', {
    extend: 'Ext.app.Controller',

    views: [
        'MenuPanel'
    ],

    routes: {
        organization: 'showOrganizations',
        'organization/:id/site': 'showSites'
    },

    refs: [
        {
            ref: 'menuPanel',
            selector: 'menu-panel'
        },
        {
            ref: 'main',
            selector: 'main'
        },
        {
            ref: 'siteGrid',
            selector: 'site-grid'
        }
    ],

    init: function () {
        var me = this;
        me.control({
            'menu-panel': {
                selectionchange: function (model, selected) {
                    if (selected.length) {
                        this.redirectTo(selected[0].get('url'));
                    }
                }
            }
        });
    },

    selectView: function (type, id) {
        this.getMain().changeTab(type, id);
    },

    showOrganizations: function () {
        this.selectView('organizations');
    },

    showSites: function (id) {
        this.getMain().changeTab('sites', { id: id });
        this.getSiteGrid().setItemId(id);
    }
});
