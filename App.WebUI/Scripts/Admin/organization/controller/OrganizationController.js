Ext.define('App.organization.controller.OrganizationController', {
    extend: 'App.base.controller.GridController',

    add: function () {
        var view = this.getView(),
            organization = this.createModel({
                id: null
            }),
            win = this.lookupReference('organization-add-window');

        if (win) {
            win.focus();

            return;
        }

        win = Ext.create('App.base.view.Window', {
            cls:       'organization-add-window',
            reference: 'organization-add-window',
            items:     [{
                xtype: 'organization-form'
            }],
            listeners: {
                createItem: function (item) {
                    view.fireEvent('createItem');
                }
            },
            viewModel: {
                xclass: 'App.organization.view.model.OrganizationModel',
                parent: this.getViewModel(),
                data:   {
                    item: organization
                }
            }
        });

        view.add(win);
        win.show();
    },

    editItem: function (record) {
        var view = this.getView(),
            form = Ext.create('App.organization.view.OrganizationForm', {
                activeRecord:     record,
                trackResetOnLoad: true
            }),
            me = this,
            win = Ext.create('App.base.view.Window', {
                cls:       'organization-edit-window-' + record.id,
                items:     [form],
                listeners: {
                    changeItem: function (changedData) {
                        view.fireEvent('changeItem', changedData);
                    }
                },
                viewModel: {
                    xclass: 'App.organization.view.model.OrganizationModel',
                    parent: me.getViewModel()
                },
                data:      {
                    item: null
                }
            });

        view.add(win);
        win.show();
        win.body.mask('Loading...');
        this.getItem(record.id)
            .then(function (record) {
                win.getViewModel().set('item', me.createModel(record.data));
            })
            .always(function () {
                win.body.unmask();
            });
    },

    getItem: function (id) {
        return this.getModel().getOneById(id);
    }
});
