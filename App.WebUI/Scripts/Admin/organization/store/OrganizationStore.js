﻿Ext.define('App.organization.store.Organizations', {
    extend: 'Ext.data.Store',
    model: 'App.organization.model.Organization',
    pageSize: 10,
    remoteFilter: true
});
