﻿Ext.define('App.organization.model.Organization', {
    extend: 'App.model.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'name' },
        { name: 'city' },
        { name: 'state' },
        { name: 'State' },
        { name: 'zip', type: 'int', convert: function (val) { return val === null ? null : parseInt(val, 10); } },
        { name: 'ownerEmail' }
    ],

    proxy: {
        type:   'api',
        url:    '/Api/Admin/Organization',
        reader: {
            rootProperty: function (raw) {
                return raw.hasOwnProperty('items') && raw.hasOwnProperty('total') ? raw.items : raw;
            }
        }
    },

    getTitle: function () {
        return this.get('name');
    }
});
