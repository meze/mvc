Ext.define('App.organization.view.model.OrganizationModel', {
    extend: 'Ext.app.ViewModel',

    data: {
        item: {}
    },

    formulas: {
        modifyTitle: function (get) {
            var name = get('item.name'),
                prefix = get('item').phantom ? 'Creating ' : 'Modifying ';

            if (typeof name !== 'string' || name.length === 0) {
                return prefix;
            }

            return prefix + name;
        },
        selected:     function (get) {
            return get('organizationGrid.selection');
        }
    },

    stores: {
        items: {
            model: 'App.organization.model.Organization'
        }
    }
});
