﻿Ext.define('App.organization.view.OrganizationGrid', {
    controller: {
        xclass: 'App.organization.controller.OrganizationController'
    },

    viewModel: {
        xclass: 'App.organization.view.model.OrganizationModel'
    },

    extend:    'App.base.view.Grid',
    xtype:     'organization-grid',
    cls:       'organization-grid',
    reference: 'organizationGrid',

    bind: '{items}',

    getColumns: function () {
        return [
            {
                text:      'Company',
                width:     300,
                dataIndex: 'name'
            },
            {
                text:      'State',
                width:     100,
                dataIndex: 'state'
            },
            {
                text:      'City',
                width:     150,
                dataIndex: 'city'
            },
            {
                text:      'Zip',
                width:     50,
                dataIndex: 'zip'
            },
            {
                text:      'Owner email',
                width:     200,
                dataIndex: 'ownerEmail'
            }
        ];
    }
});
