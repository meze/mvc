﻿Ext.define('App.organization.view.OrganizationForm', {
    extend:        'Ext.form.Panel',
    border:        false,
    baseCls:       'x-plain',
    fieldDefaults: {
        labelAlign: 'right',
        layout:     'anchor',
        anchor:     '100%',
        labelWidth: 90
    },
    paramOrder:    ['id'],
    defaultType:   'textfield',
    bodyPadding:   5,

    xtype: 'organization-form',
    cls:   'organization-form',

    config: {
        activeRecord: null
    },

    items: [
        {
            xtype: 'hidden',
            bind:  '{item.id}',
            name:  'id',
            rawToValue: function (val) {
                return parseInt(val, 10);
            }
        },
        {
            fieldLabel:          'Name',
            name:                'name',
            bind:                '{item.name}',
            allowBlank:          false,
            allowOnlyWhitespace: false
        }, {
            fieldLabel: 'Owner e-mail',
            name:       'ownerEmail',
            vtype:      'email',
            bind:       '{item.ownerEmail}',
            anchor:     '100%'
        }, {
            xtype:       'container',
            layout:      'hbox',
            baseCls:     'x-plain',
            defaultType: 'textfield',
            defaults:    {
                labelAlign: 'right',
                anchor:     '100%'
            },

            items: [{
                fieldLabel: 'City',
                labelWidth: 90,
                flex:       1,
                name:       'city',
                bind:       '{item.city}'
            }, {
                fieldLabel: 'State',
                labelWidth: 45,
                width:      200,
                name:       'state',
                bind:       '{item.state}'
            }, {
                fieldLabel: 'Zip',
                labelWidth: 30,
                width:      110,
                name:       'zip',
                maskRe:     /[0-9]/,
                regex:      /^[0-9]+$/,
                regexText:  'Must contain only integers',
                bind:       '{item.zip}'
            }]
        }
    ]
});
