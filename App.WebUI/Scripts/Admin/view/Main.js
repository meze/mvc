Ext.define('App.view.Main', {
    extend:        'Ext.tab.Panel',
    xtype:         'main',
    titleRotation: 0,
    tabRotation:   0,
    tabBar:        {
        flex:   1,
        layout: {
            align:           'stretch',
            overflowHandler: 'none'
        }
    },

    config: {
        menu: null
    },

    changeTab: function (type, params) {
        var map = {
            organizations: 1,
            organization:  1,
            sites:         2
        };

        if (!map[type]) {
            return;
        }

        this.getActiveTab().tab.hide();
        this.setActiveTab(map[type]);
        this.getActiveTab().tab.show();
        this.getActiveTab().fireEvent('page.show', params);
    },

    initComponent: function () {
        var me = this,
            menu = this.menu;
        Ext.apply(me, {
            items: [{
                xtype:     'component',
                tabConfig: {
                    hidden: true
                }
            }, {
                iconCls:     'x-icon-grid',
                title:       'Organizations',
                collapsible: false,
                region:      'center',
                tabConfig:   {
                    hidden: true
                },
                xtype:       'organization-grid',
                listeners:   {
                    changeItem:  function (changedData) {
                        if (menu && changedData.hasOwnProperty('name')) {
                            menu.fireEvent('treeChange', 'organizations/');
                        }
                    },
                    createItem:  function () {
                        if (menu) {
                            menu.fireEvent('treeChange', 'organizations/');
                        }
                    },
                    removeItems: function () {
                        if (menu) {
                            menu.fireEvent('treeChange', 'organizations/');
                        }
                    }
                }
            }, {
                title:       'Sites',
                collapsible: false,
                region:      'center',
                hidden:      true,
                xtype:       'site-grid'
            }]
        });
        me.callParent(arguments);
    }
});
