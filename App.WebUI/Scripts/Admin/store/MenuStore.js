﻿Ext.define('store.Menus', {
    extend: 'Ext.data.TreeStore',
    model: 'model.Menu',

    root: {
        expanded: true
    },
    proxy: {
        url:        '/Api/Admin/NavigationTree',
        type:       'api',
        paramOrder: ['node']
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (this.body){
                this.body.mask();
            }
        },
        load: function(){
            if (this.body){
                this.body.unmask();
            }
        },
        scope: 'this'
    }
});
