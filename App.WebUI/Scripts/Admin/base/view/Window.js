Ext.define('App.base.view.Window', {
    extend: 'Ext.window.Window',

    bind:      {
        title: '{modifyTitle:htmlEncode}'
    },

    listeners: {
        show: function () {
            this.fitContent();
        }
    },

    width:     600,
    height:    10,
    layout:    'fit',
    plain:     true,
    constrainHeader: true,

    buttons: [{
        text:    'Save',
        cls:     'save-btn',
        width:   90,
        iconCls: 'x-icon-tick',
        handler: 'save'
    }, {
        text:    'Cancel',
        iconCls: 'x-icon-cancel',
        handler: 'cancelAdd'
    }],

    initComponent: function () {
        var maxHeight = Ext.getBody().getHeight();
        var maxWidth = Ext.getBody().getWidth();

        Ext.apply(this, {
            maxHeight: maxHeight,
            maxWidth:  maxWidth,
            items:     [{
                xtype:  'container',
                itemId: 'window-container',
                items:  this.items
            }]
        });

        this.callParent();
    },

    fitContent: function () {
        var maxHeight = Ext.getBody().getHeight();
        var maxWidth = Ext.getBody().getWidth();
        var self = this,
            plusHeight = 90,
            scrollHeight = self.down('#window-container').getEl().dom.scrollHeight,
            newHeight = scrollHeight + plusHeight;

        if (newHeight > self.getHeight() && newHeight < maxHeight) {
            self.setHeight(newHeight);
        }

        if (newHeight > maxHeight) {
            self.setHeight(maxHeight);
        }

        if (self.getWidth() > maxWidth) {
            self.setWidth(maxWidth);
        }

        self.center();
        self.doLayout();
    }
});
