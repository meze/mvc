Ext.define('App.base.view.Grid', {
    extend: 'Ext.grid.Panel',

    controller: {
        xclass: 'App.base.controller.GridController'
    },

    collapsible: true,
    multiSelect: true,

    viewConfig: {
        enableTextSelection: true
    },

    reference: 'base-grid',

    listeners: {
        'page.show': 'onShow',
        rowdblclick: 'edit'
    },

    initComponent: function () {
        var self = this,
            actions = this.createActions(),
            contextMenu = Ext.create('Ext.menu.Menu', {
                items: actions,
                cls:   'grid-context-menu'
            });

        Ext.apply(this, {
            items: [
                contextMenu
            ],
            // grid columns
            bbar: Ext.create('Ext.PagingToolbar', {
                displayInfo: true,
                store:       this.getViewModel().getStore('items'),
                displayMsg:  'Displaying items {0} - {1} of {2}',
                emptyMsg:    "Nothing to display"
            }),
            columns:    this.getColumns().concat([{
                menuDisabled: true,
                sortable:     false,
                xtype:        'actioncolumn',
                width:        36,
                items:        [{
                    iconCls: 'x-icon-edit',
                    tooltip: 'Edit',
                    handler: 'chooseAndEdit'
                }, {
                    iconCls: 'x-icon-delete',
                    tooltip: 'Delete',
                    handler: 'chooseAndRemove'
                }]
            }]),
            tbar:       actions.concat([
                '->',
                {
                    width:      400,
                    fieldLabel: 'Search',
                    labelWidth: 50,
                    margin:     '0 5 0 0',
                    xtype:      'searchfield',
                    store:      this.getViewModel().getStore('items'),
                    itemId:     'grid-search',
                    cls:        'grid-basic-search'
                }
            ]),
            selType:    'checkboxmodel',
            loadMask:   true,
            minWidth:   300,
            viewConfig: {
                stripeRows: true,
                listeners:  {
                    itemcontextmenu: function (view, rec, node, index, e) {
                        e.stopEvent();

                        contextMenu.showBy(node, 'tl-tl', [e.getX() - view.getX(), 9]);
                        return false;
                    }
                }
            }
        });

        this.callParent();

        self.add(contextMenu);

        this.toggleActionsButtons([]);
        this.getSelectionModel().on('selectionchange', function (model, selections) {
            console.log(selections);
            this.toggleActionsButtons(selections);
        }, this);
    },

    toggleActionsButtons: function(selections) {
        this.down('#delete').setDisabled(selections.length === 0);
        this.down('#edit').setDisabled(selections.length === 0);
    },

    createActions: function () {
        return [
            Ext.create('Ext.Action', {
                text:    'Add',
                handler: 'add',
                iconCls: 'x-icon-add'
            }),
            Ext.create('Ext.Action', {
                text:    'Edit',
                handler: 'edit',
                iconCls: 'x-icon-edit',
                itemId:  'edit',
                cls:     'edit-btn'
            }),
            Ext.create('Ext.Action', {
                text:    'Delete',
                handler: 'remove',
                iconCls: 'x-icon-delete',
                itemId:  'delete',
                cls:     'delete-btn'
            })
        ];
    },

    getColumns: function () {
        return [];
    },

    afterShow: function () {
        this.callParent(arguments);
        this.down('#grid-search').resetFilter();
    }
});
