﻿Ext.define('App.model.Model', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    save: function (options) {
        var dfd = $.Deferred();
        options = Ext.merge({}, options, {
            success: function () {
                dfd.resolveWith(this, arguments);
            },
            failure: function () {
                dfd.rejectWith(this, arguments);
            },
            scope:   this
        });

        this.callParent([options]);

        return dfd.promise();
    },

    getTitle: function () {
        return this.get('id');
    },

    inheritableStatics: {
        getOneById: function (id) {
            var dfd = $.Deferred();

            this.load(id, {
                success: function () {
                    dfd.resolveWith(this, arguments);
                },
                failure: function () {
                    dfd.rejectWith(this, arguments);
                },
                scope:   this
            });

            return dfd.promise();
        }
    }
});


