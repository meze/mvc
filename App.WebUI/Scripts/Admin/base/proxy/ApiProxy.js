Ext.define('App.base.proxy.ApiProxy', {
    extend: 'Ext.data.proxy.Rest',
    alias:  'proxy.api',

    reader:         {
        type:            'json',
        successProperty: 'id',
        totalProperty:   'total'
    },
    writer:         {
        writeAllFields: true
    },
    simpleSortMode: true,
    paramOrder:     'page|start|perPage|filter',
    limitParam:     'perPage',

    encodeFilters: function (filters) {
        return filters[0].value;
    },

    buildUrl: function (request) {
        var values = this.extractValues(this.getUrl(), request),
            url =  this.callParent(arguments);

        return this.replaceTokens(url, values);
    },

    extractValues: function (str, request) {
        var params = request.getParams(),
            regexp = /\{([a-z0-9_]+)\}/ig,
            values = {},
            match = regexp.exec(str);

        while (match !== null) {
            values[match[0]] = params[match[1]];
            delete params[match[1]];
            match = regexp.exec(str);
        }

        return values;
    },

    replaceTokens: function (str, values) {
        Ext.iterate(values, function (token, val) {
            str = str.replace(token, encodeURIComponent(val));
        });

        return str;
    }
});

