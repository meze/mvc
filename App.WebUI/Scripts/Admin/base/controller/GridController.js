Ext.define('App.base.controller.GridController', {
    extend:        'Ext.app.ViewController',
    requestParams: {},

    add: function () {
        throw new Error('Add method must be implemented in the child classes');
    },

    chooseAndEdit: function (grid, rowIndex) {
        var rec = grid.getStore().getAt(rowIndex);
        grid.getSelectionModel().doSelect(rec);
        this.edit(rec);
    },

    chooseAndRemove: function (grid, rowIndex, colIndex, btn, row) {
        /*jslint unparam:true*/
        var rec = grid.getStore().getAt(rowIndex);
        grid.getSelectionModel().doSelect(rec);
        this.remove(row.position.cellElement);
    },

    edit: function () {
        var selections = this.getView().getSelectionModel().getSelection();
        Ext.each(selections, function (selection) {
            this.editItem(selection);
        }, this);
    },

    editItem: function (item) {
        /*jslint unparam:true*/
        throw new Error('Edit method must be implemented in the child classes');
    },

    save: function (btn) {
        var me = this,
            win = btn.up('window'),
            formPanel = win.down('form'),
            item = win.getViewModel().get('item'),
            changedData = item.modified || {},
            isPhantom = item.phantom,
            activeRecord;

        if (formPanel.isValid()) {
            btn.disable();
            win.body.mask('Saving...');

            activeRecord = formPanel.getActiveRecord();
            // sync data to update id for new records
            formPanel.updateRecord(item);
            win.getViewModel().get('item').save({
                params: this.requestParams
            }).then(function () {
                if (isPhantom) {
                    me.insertNew(this.getData());
                    win.fireEvent('createItem', this.getData());
                } else {
                    //activeRecord = formPanel.getActiveRecord();
                    formPanel.updateRecord(activeRecord);
                    activeRecord.commit();
                    win.fireEvent('changeItem', changedData);
                }
                win.body.unmask();
                win.destroy();
                win = null;
            }, function (form, action) {
                var response = Ext.JSON.decode(action.error.response.responseText);
                if (response && response.errors) {
                    formPanel.getForm().markInvalid(response.errors);
                }
                me.onFormError();
            }).always(function () {
                if (win) {
                    win.body.unmask();
                    btn.enable();
                }
            });
        } else {
            me.onFormError();
        }
    },

    insertNew: function (item) {
        this.getStore('items').insert(0, item);
    },

    createModel: function (data) {
        return Ext.create(this.getModel(), data);
    },

    getModel: function () {
        return this.getStore('items').getModel();
    },

    onShow: function () {
        this.getStore('items').load();
    },

    cancelAdd: function (btn) {
        btn.up('window').destroy();
    },

    remove: function (animtationTarget) {
        var msg,
            items = [],
            selections = this.getView().getSelectionModel().getSelection();

        Ext.each(selections, function (selection) {
            items.push(selection);
        }, this);

        if (items.length === 1) {
            msg = '“' + items[0].getTitle() + '”';
        } else {
            msg = items.length + ' items';
        }

        Ext.MessageBox.show({
            cls:           'remove-grid-item-confirm',
            title:         'About to Remove',
            msg:           'Are you sure you want to remove ' + msg + '?',
            buttons:       Ext.MessageBox.YESNO,
            scope:         this,
            animateTarget: animtationTarget,
            icon:          Ext.MessageBox.QUESTION,
            fn:            function (answer) {
                if (answer === 'yes') {
                    this.removeItems(items);
                }
            }
        });
    },

    removeItems: function (items) {
        var store = this.getStore('items'),
            tasks = [],
            self = this;
        self.getView().mask('Removing...');

        Ext.each(items, function (item) {
            tasks.push(item.erase().then(function () {
                store.commitChanges();
            }).fail(function () {
                store.rejectChanges();
            }));
        }, self);

        $.when.apply($, tasks).always(function () {
            self.getView().unmask();
            self.getView().fireEvent('removeItems', items);
        });
    },

    onFormError: function () {
        Ext.toast({
            html:            'Please correct the errors in the form ' +
            'and submit it again',
            align:           't',
            slideInDuration: 400,
            minWidth:        400,
            title:           'Something Wrong',
            iconCls:         'x-icon-error',
            cls:             'form-error-toast'
        });
    }
});
