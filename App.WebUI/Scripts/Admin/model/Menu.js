﻿Ext.define('model.Menu', {
    extend: 'Ext.data.TreeModel',
    fields: [
        { name: 'id', type: 'string' },
        { name: 'name' }
    ]
});