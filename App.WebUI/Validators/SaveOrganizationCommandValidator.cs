﻿using App.Domain.Organizations.Commands;
using FluentValidation;

namespace App.WebUI.Validators
{
    public class SaveOrganizationCommandValidator : AbstractValidator<SaveOrganizationCommand>
    {
        public SaveOrganizationCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(2, int.MaxValue);
        }
    }
}