using App.Domain.Concrete;
using App.Domain.Entities;
using App.Domain.Entities.Navigation;
using App.Domain.Entities.Structure;
using App.Domain.Helpers;
using App.Domain.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace App.WebUI.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<App.Domain.Concrete.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}