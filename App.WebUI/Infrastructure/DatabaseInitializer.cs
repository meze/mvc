﻿using App.Domain.Concrete;
using App.Domain.Entities;
using App.Domain.Entities.Navigation;
using App.Domain.Entities.Structure;
using App.Domain.Helpers;
using App.Domain.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace App.WebUI.Infrastructure
{
    public class DatabaseInitializer : SeedDatabase<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            var deleter = new DbCleaner(context);
            deleter.DeleteAllData();

            var site = InitializeSite(context);

            InitializeOrganization(site, context);

            InitializeNewsModule(context, site);

            InitializeNav(context);
            base.Seed(context);
        }

        private static void InitializeNewsModule(EFDbContext context, Site site)
        {
            var module = new Module
            {
                Id = 1,
                Name = "News",
                Site = site
            };

            var dateField = new Field
            {
                Name = "date",
                Type = FieldType.DATETIME
            };

            var titleField = new Field
            {
                Name = "title",
                Type = FieldType.STRING
            };

            module.AddField(dateField);
            module.AddField(titleField);

            var row = new Row
            {
                Id = 1,
                Module = module
            };
            var row2 = new Row
            {
                Id = 2,
                Module = module
            };

            module.AddField(titleField);

            context.Modules.AddOrUpdate(m => m.Id, module);

            row.AddValue(dateField, DateTimeOffset.Now.ToString());
            row.AddValue(titleField, "Will it blend");

            row2.AddValue(dateField, DateTimeOffset.Now.ToString());
            row2.AddValue(titleField, "Will it blend");

            context.Rows.AddOrUpdate(r => r.Id, row);
            context.Rows.AddOrUpdate(r => r.Id, row2);
        }

        private static Site InitializeSite(EFDbContext context)
        {
            var config = new Config
            {
                BackgroundColor = "#ffffff"
            };

            var site = new Site
            {
                Domain = "localhost",
                Title = "Example",
                Config = config
            };
            context.Sites.AddOrUpdate(s => s.Id, site);

            return site;
        }

        private static void InitializeOrganization(Site site, EFDbContext context)
        {
            var organization = new Organization("Mgp Solutions")
            {
                City = "Atlanta",
                Zip = "46031",
                OwnerEmail = "demo@example.com",
                State = "Indiana",
                Id = 1
            };
            organization.AddSite(site);

            var secondOrganization = new Organization("Acme Company")
            {
                City = "Knightstown",
                Zip = "46148",
                OwnerEmail = "demo2@example.com",
                State = "Indiana",
                Id = 2
            };

            context.Organizations.AddOrUpdate(o => o.Id, organization);
            context.Organizations.AddOrUpdate(o => o.Id, secondOrganization);
        }

        private static void InitializeNav(EFDbContext context)
        {
            var menu = new Menu
            {
                Id = 1,
                Type = MenuType.DESKTOP
            };

            menu.AddItem(new MenuItem() { Id = 1, Link = "", Path = "/" });
            menu.AddItem(new MenuItem() { Id = 2, Link = "/", Path = "/1" });
            menu.AddItem(new MenuItem() { Id = 3, Link = "/about", Path = "/1" });
            menu.AddItem(new MenuItem() { Id = 4, Link = "/about/contact", Path = "/1/3" });

            context.Menus.AddOrUpdate(m => m.Id, menu);
        }
    }
}