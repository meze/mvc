﻿using FluentValidation;
using Ninject;
using Ninject.Planning.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace App.WebUI.Infrastructure
{
    public class FluentValidationConfig : ValidatorFactoryBase
    {
        private IKernel _kernel;

        public FluentValidationConfig(IKernel kernel)
        {
            _kernel = kernel;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return (validatorType == null) ? null : (IValidator)_kernel.TryGet(validatorType);
        }
    }
}