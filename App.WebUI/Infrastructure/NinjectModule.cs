﻿using App.Core.Cqrs;
using App.Core.Cqrs.Concrete;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using App.Domain.Sites.Queries;
using App.WebUI.Filters;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;
using Ninject.Web.Common;
using NLog;
using System.Configuration;
using System.Data.Entity;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace App.WebUI.Infrastructure
{
    public class AppModule : NinjectModule
    {
        public override void Load()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Bind<EFDbContext>()
                .To<EFDbContext>()
                .InRequestScope();

            Bind<IQueryDispatcher>().To<QueryDispatcher>().InRequestScope();
            Bind<ICommandDispatcher>().To<CommandDispatcher>().InRequestScope();
            Bind<IHandlerFactory>().To<NinjectHandlerFactory>().InRequestScope();
            Bind<DomainDetectionFilter>().To<DomainDetectionFilter>().WithConstructorArgument(403);
            Bind<Site>().ToMethod(context =>
            {
                var queryDispatcher = Kernel.TryGet<IQueryDispatcher>();
                return queryDispatcher.Dispatch<FindSingleSiteByDomainQuery, Site>(new FindSingleSiteByDomainQuery
                {
                    Domain = HttpContext.Current.Request.Url.Host.ToLower()
                });
            }).WhenInjectedInto<IController>();

            Kernel.Bind(x => x
                .FromAssembliesMatching("App.Domain.dll")
                .SelectAllClasses().InheritedFrom(typeof(IQueryHandler<,>))
                .BindAllInterfaces());

            Kernel.Bind(x => x
                .FromAssembliesMatching("App.Domain.dll")
                .SelectAllClasses().InheritedFrom(typeof(ICommandHandler<>))
                .BindAllInterfaces());

            Bind<Logger>().ToMethod(context =>
            {
                return LogManager.GetLogger(context.Request.Target.Member.DeclaringType.FullName);
            });
        }
    }
}