﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using App.Domain.Concrete;
using App.Domain.Entities;
using App.Domain.Entities.Navigation;
using App.Domain.Entities.Structure;
using App.Domain.Helpers;
using App.Domain.Models;

namespace App.WebUI.Infrastructure
{
    public class EmptyDatabaseInitializer : NullDatabaseInitializer<EFDbContext>
    {
        public override void InitializeDatabase(EFDbContext context)
        {
            var cleaner = new DbCleaner(context);
            cleaner.DeleteAllData();

            base.InitializeDatabase(context);
        }
    }
}