﻿using App.Core.Cqrs;
using Ninject;

namespace App.WebUI.Infrastructure
{
    public class NinjectHandlerFactory : IHandlerFactory
    {
        private IKernel _kernel;

        public NinjectHandlerFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public T Get<T>()
        {
            return _kernel.TryGet<T>();
        }
    }
}