﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Serialization;

namespace App.WebUI.Infrastructure
{
    public class LowerCaseContractResolver: DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            if (String.IsNullOrEmpty(propertyName))
            {
                return propertyName;
            }

            return Char.ToLowerInvariant(propertyName[0]) + propertyName.Substring(1);
        }
    }
}