﻿using System.Configuration;

namespace App.WebUI.Infrastructure
{
    public class LoggerSettings : ConfigurationSection
    {
        private static LoggerSettings settings = ConfigurationManager.GetSection("LoggerSettings") as LoggerSettings;

        public static LoggerSettings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("eventId", IsRequired = false)]
        public string EventId
        {
            get { return (string)this["eventId"]; }
            set { this["eventId"] = value; }
        }

        [ConfigurationProperty("eventId", IsRequired = false)]
        public string LogSeverity
        {
            get { return (string)this["eventId"]; }
            set { this["eventId"] = value; }
        }
    }
}