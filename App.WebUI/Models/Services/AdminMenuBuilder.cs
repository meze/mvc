﻿using App.Domain.Entities.Structure;
using App.WebUI.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.WebUI.Models.Services
{
    public class AdminMenuBuilder
    {
        public List<MenuItemViewModel> BuildFor(IEnumerable<Organization> organizations)
        {
            List<MenuItemViewModel> result = new List<MenuItemViewModel>();
            var organizationNode = new MenuItemViewModel
            {
                Url = "organization",
                Text = "Organizations",
                Children = new List<MenuItemViewModel>()
            };

            result.Add(organizationNode);

            foreach (var organization in organizations)
            {
                var children = new List<MenuItemViewModel>();
                var sites = new List<MenuItemViewModel>();
                foreach (var site in organization.Sites)
                {
                    sites.Add(new MenuItemViewModel
                    {
                        Url = "organization/" + organization.Id + "/site/" + site.Id,
                        Text = site.Title,
                        Leaf = true,
                        Qtip = site.Domain
                    });
                }

                children.Add(new MenuItemViewModel
                {
                    Url = "organization/" + organization.Id + "/site",
                    Text = "Sites",
                    IconCls = "x-icon-world",
                    Leaf = organization.Sites.Count == 0,
                    Children = sites
                });

                organizationNode.Children.Add(new MenuItemViewModel
                {
                    Url = "organization/" + organization.Id,
                    Text = organization.Name,
                    Children = children,
                    IconCls = "x-icon-building",
                });
            }

            return result;
        }
    }
}