﻿using System.Linq;
using System.Web.Mvc;
using App.Core.Cqrs;
using App.Domain.Abstract;
using App.Domain.Entities.Navigation;
using App.Domain.Entities.Structure;
using App.Domain.Menus.Queries;
using App.Domain.Models.Tree;

namespace App.WebUI.Controllers 
{
    public class NavigationController : Controller 
    {
        private Site _site;
        private IQueryDispatcher _queryDispatcher;

        public NavigationController(Site site, IQueryDispatcher queryDispatcher)
        {
            _site = site;
            _queryDispatcher = queryDispatcher;
        }

        public ActionResult Index()
        {
            var menu = _queryDispatcher.Dispatch<SingleMenuQuery, Menu>(new SingleMenuQuery(1));

            var tree = TreeBuilder.Build<MenuItem>(menu.Items.ToList());

            return PartialView(tree);
        }
    }
}
