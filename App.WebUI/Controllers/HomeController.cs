﻿using System.Collections.Generic;
using System.Web.Mvc;
using App.Core.Cqrs;
using App.Domain.Entities;
using App.Domain.Entities.Structure;
using App.Domain.Rows.Queries;

namespace App.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private Site _site;
        private IQueryDispatcher _queryDispatcher;

        public HomeController(Site site, IQueryDispatcher queryDispatcher)
        {
            _site = site;
            _queryDispatcher = queryDispatcher;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            ViewBag.Site = _site;

            var rows = _queryDispatcher.Dispatch<AllRowsQuery, IEnumerable<Row>>(new AllRowsQuery());

            return View(rows);
        }

        public JsonResult Row(int id)
        {
            var row = _queryDispatcher.Dispatch<SingleRowQuery, Row>(new SingleRowQuery
            {
                Id = id
            });
            return Json(row.ExportValues(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
