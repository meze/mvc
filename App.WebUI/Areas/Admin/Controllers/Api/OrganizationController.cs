﻿using App.Core.Cqrs;
using App.Domain.Entities.Structure;
using App.Domain.Models;
using App.Domain.Organizations.Commands;
using App.Domain.Organizations.Queries;
using App.WebUI.Areas.Admin.Models;
using App.WebUI.Services;
using App.WebUI.Validators;
using AutoMapper;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.WebUI.Areas.Admin.Controllers.Api
{
    public class Test
    {
        public string Page { get; set; }
    }

    public class OrganizationController : ApiController
    {
        private IQueryDispatcher _queryDispatcher;
        private ICommandDispatcher _commandDispatcher;

        public OrganizationController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet]
        public PagedResult<OrganizationViewModel> Get([FromUri] SearchOrganizationsQuery query)
        {
            var result = _queryDispatcher.Dispatch<SearchOrganizationsQuery, PagedResult<Organization>>(query);

            return Mapper.Map<PagedResult<Organization>, PagedResult<OrganizationViewModel>>(result);
        }

        [HttpGet]
        public OrganizationViewModel Get(int id)
        {
            var organization = _queryDispatcher.Dispatch<SingleOrganizationQuery, Organization>(new SingleOrganizationQuery { Id = id });
            return Mapper.Map<Organization, OrganizationViewModel>(organization);
        }

        [HttpPut]
        [HttpPost]
        public HttpResponseMessage Save(SaveOrganizationCommand command)
        {
            var validator = new SaveOrganizationCommandValidator();

            var validationResult = validator.Validate(command);

            if (validationResult.IsValid == false)
            {
                var errorConverter = new ValidatorErrorConverter();
                errorConverter.Convert(validationResult.Errors);

                return Request.CreateResponse(HttpStatusCode.BadRequest, new { errors = errorConverter.Convert(validationResult.Errors) });
                // return Request.cre
                // return BadRequest(validationResult.Errors)
            }

            _commandDispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.OK, Get(command.Id));
        }

        [HttpDelete]
        public void Delete(int id)
        {
            var command = new RemoveOrganizationsCommand();
            command.IdsToRemove.Add(id);

            _commandDispatcher.Dispatch(command);
        }
    }
}