﻿using App.Core.Cqrs;
using App.Domain.Entities.Structure;
using App.Domain.Organizations.Queries;
using App.WebUI.Areas.Admin.Models;
using App.WebUI.Models.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace App.WebUI.Areas.Admin.Controllers.Api
{
    [RoutePrefix("Api/Admin/NavigationTree")]
    public class NavigationTreeController : ApiController
    {
        private IQueryDispatcher _queryDispatcher;

        public NavigationTreeController(IQueryDispatcher queryDispatcher)
        {
            _queryDispatcher = queryDispatcher;
        }

        [Route("{node}")]
        public List<MenuItemViewModel> Get(string node)
        {
            var builder = new AdminMenuBuilder();
            var organizations = _queryDispatcher.Dispatch<AllOrganizationsQuery, IEnumerable<Organization>>(new AllOrganizationsQuery());

            return builder.BuildFor(organizations);
        }
    }
}