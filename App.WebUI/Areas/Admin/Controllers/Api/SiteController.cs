﻿using App.Core.Cqrs;
using App.Domain.Entities.Structure;
using App.Domain.Models;
using App.Domain.Organizations.Queries;
using App.Domain.Sites.Commands;
using App.Domain.Sites.Queries;
using App.WebUI.Areas.Admin.Models;
using AutoMapper;
using System.Web.Http;

namespace App.WebUI.Areas.Admin.Controllers.Api
{
    //[Authorize(Roles = "SuperAdmin")]
    [RoutePrefix("Api/Admin/Organization")]
    public class SiteController : ApiController
    {
        private ICommandDispatcher _commandDispatcher;
        private IQueryDispatcher _queryDispatcher;

        public SiteController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [Route("{organizationId}/Site")]
        public PagedResult<SiteViewModel> Get(int organizationId, int page = 1, int start = 0, int limit = 10, string query = null)
        {
            var organization = GetOrganization(organizationId);
            var sites = _queryDispatcher.Dispatch<SearchSitesQuery, PagedResult<Site>>(new SearchSitesQuery
            {
                Organization = organization,
                Page = page,
                PerPage = limit,
                Filter = query,
            });

            return Mapper.Map<PagedResult<Site>, PagedResult<SiteViewModel>>(sites);
        }

        [Route("{organizationId}/Site/{id}")]
        public SiteViewModel Get(int organizationId, int id)
        {
            var site = _queryDispatcher.Dispatch<SingleSiteQuery, Site>(new SingleSiteQuery { Id = id });

            return Mapper.Map<Site, SiteViewModel>(site);
        }

        [HttpPut]
        [HttpPost]
        [Route("{organizationId}/Site")]
        [Route("{organizationId}/Site/{id}")]
        public SiteViewModel Save(int organizationId, SaveSiteCommand command)
        {
            command.Organization = GetOrganization(organizationId);
            _commandDispatcher.Dispatch(command);

            return Get(command.Organization.Id, command.Id);
        }

        [HttpDelete]
        [Route("{organizationId}/Site/{id}")]
        public void Delete(int id)
        {
            var command = new RemoveSitesCommand();
            command.IdsToRemove.Add(id);

            _commandDispatcher.Dispatch(command);
        }

        private Organization GetOrganization(int organizationId)
        {
            return _queryDispatcher.Dispatch<SingleOrganizationQuery, Organization>(new SingleOrganizationQuery { Id = organizationId });
        }
    }
}