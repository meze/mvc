﻿using System.Text;
using System.Web.Mvc;
using App.WebUI.Infrastructure;
using Ext.Direct.Mvc;
using Newtonsoft.Json;

namespace App.WebUI.Areas.Admin.Controllers
{
    public abstract class BaseController : DirectController
    {
        protected override DirectResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior, JsonSerializerSettings settings)
        {
            if (settings == null)
            {
                settings = new JsonSerializerSettings();
            }

            settings.ContractResolver = new LowerCaseContractResolver();

            return base.Json(data, contentType, contentEncoding, behavior, settings);
        }
    }
}