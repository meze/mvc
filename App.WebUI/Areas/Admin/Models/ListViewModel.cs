﻿using System.Collections.Generic;

namespace App.WebUI.Areas.Admin.Models
{
    public class ListViewModel<T>
    {
        public int Total { get; set; }

        public IEnumerable<T> Items { get; set; }
    }
}