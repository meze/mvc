﻿namespace App.WebUI.Areas.Admin.Models
{
    public class SiteViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Domain { get; set; }

        public string BasePath { get; set; }

        public string OwnerEmail { get; set; }
    }
}