﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.WebUI.Filters
{
    public class DomainDetectionFilter: IActionFilter
    {
        private int status;
        public DomainDetectionFilter(int test)
        {
            status = test;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

            HttpRequestBase request = filterContext.HttpContext.Request;


            var context = filterContext.HttpContext;
            if (context != null)
            {
                context.Response.Write(request.Url.Host);
            }

            //throw new HttpException(status, "Not found");
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // empty
        }
    }
}