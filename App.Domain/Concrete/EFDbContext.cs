﻿using App.Domain.Entities;
using App.Domain.Entities.Navigation;
using App.Domain.Entities.Structure;
using System;
using System.Data.Entity;

namespace App.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Row> Rows { get; set; }

        public DbSet<Site> Sites { get; set; }

        public DbSet<Module> Modules { get; set; }

        public DbSet<Field> Fields { get; set; }

        public DbSet<RowValue> RowValues { get; set; }

        public DbSet<Config> Configs { get; set; }

        public DbSet<MenuItem> MenuItems { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<Organization> Organizations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}