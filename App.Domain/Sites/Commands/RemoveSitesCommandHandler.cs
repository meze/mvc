﻿using App.Core.Cqrs;
using App.Domain.Concrete;
using System.Linq;

namespace App.Domain.Sites.Commands
{
    public class RemoveSitesCommandHandler : ICommandHandler<RemoveSitesCommand>
    {
        private EFDbContext _context;

        public RemoveSitesCommandHandler(EFDbContext context)
        {
            _context = context;
        }

        public void Execute(RemoveSitesCommand command)
        {
            var itemsToDelete = _context.Sites.Where(x => command.IdsToRemove.Contains(x.Id));
            _context.Sites.RemoveRange(itemsToDelete);
            _context.SaveChanges();
        }
    }
}