﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;

namespace App.Domain.Sites.Commands
{
    public class SaveSiteCommandHandler : ICommandHandler<SaveSiteCommand>
    {
        private EFDbContext _context;

        public SaveSiteCommandHandler(EFDbContext context)
        {
            _context = context;
        }

        public void Execute(SaveSiteCommand command)
        {
            Site site;
            bool isNew = false;

            if (command.Id > 0)
            {
                site = _context.Sites.SingleOrDefault(o => o.Id == command.Id);
            }
            else
            {
                isNew = true;
                site = new Site();
            }

            site.Title = command.Title;
            site.OwnerEmail = command.OwnerEmail;
            site.Organization = command.Organization;
            site.Domain = command.Domain;

            if (isNew)
            {
                _context.Sites.Add(site);
            }
            _context.SaveChanges();

            command.Id = site.Id;
        }
    }
}
