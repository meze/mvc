﻿using App.Core.Cqrs;
using App.Domain.Entities.Structure;

namespace App.Domain.Sites.Commands
{
    public class SaveSiteCommand : ICommand
    {
        public int Id { get; set; }
        public string Domain { get; set; }
        public string Title { get; set; }
        public string OwnerEmail { get; set; }
        public Organization Organization { get; set; }
    }
}
