﻿using App.Core.Cqrs;
using System.Collections.Generic;

namespace App.Domain.Sites.Commands
{
    public class RemoveSitesCommand : ICommand
    {
        private IList<int> IdList = new List<int>(1);

        public IList<int> IdsToRemove
        {
            get
            {
                return IdList;
            }
        }
    }
}