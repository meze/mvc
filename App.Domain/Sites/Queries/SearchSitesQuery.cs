﻿using App.Core.Cqrs;
using App.Domain.Abstract;
using App.Domain.Entities.Structure;

namespace App.Domain.Sites.Queries
{
    public class SearchSitesQuery : SearchQuery, IQuery
    {
        public Organization Organization { get; set; }
    }
}
