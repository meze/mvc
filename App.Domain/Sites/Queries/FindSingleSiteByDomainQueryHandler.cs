﻿using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using System.Linq;

namespace App.Domain.Sites.Queries
{
    public class FindSingleSiteByDomainQueryHandler : IQueryHandler<FindSingleSiteByDomainQuery, Site>
    {
        private EFDbContext _context;

        public FindSingleSiteByDomainQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public Site Retrieve(FindSingleSiteByDomainQuery query)
        {
            return _context.Sites.SingleOrDefault(s => s.Domain == query.Domain);
        }
    }
}