﻿using App.Core.Cqrs;
using App.Domain.Abstract;
using App.Domain.Entities.Structure;

namespace App.Domain.Sites.Queries
{
    public class FindSingleSiteByDomainQuery : IQuery
    {
        public string Domain { get; set; }
    }
}
