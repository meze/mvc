﻿using System.Linq;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using App.Domain.Models;
using EntityFramework.Extensions;

namespace App.Domain.Sites.Queries
{
    public class SearchSitesQueryHandler : IQueryHandler<SearchSitesQuery, PagedResult<Site>>
    {
        private EFDbContext _context;

        public SearchSitesQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public PagedResult<Site> Retrieve(SearchSitesQuery query)
        {
            IQueryable<Site> q = _context.Sites.Where(o => o.Organization.Id == query.Organization.Id);

            if (query.HasQuery)
            {
                q = q.Where(o => o.Title.Contains(query.Filter));
            }

            q = q.OrderBy(o => o.Id);

            var totalQuery = q.FutureCount();
            var resultQuery = q.Skip(query.SkipNumber).Take(query.PerPage).Future();

            return new PagedResult<Site>(resultQuery.ToList(), totalQuery.Value);
        }
    }
}
