﻿using App.Core.Cqrs;

namespace App.Domain.Sites.Queries
{
    public class SingleSiteQuery : IQuery
    {
        public int Id { get; set; }
    }
}
