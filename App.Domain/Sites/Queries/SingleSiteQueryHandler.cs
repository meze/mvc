﻿using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using System.Linq;

namespace App.Domain.Sites.Queries
{
    public class SingleSiteQueryHandler : IQueryHandler<SingleSiteQuery, Site>
    {
        private EFDbContext _context;

        public SingleSiteQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public Site Retrieve(SingleSiteQuery query)
        {
            return _context.Sites.SingleOrDefault(o => o.Id == query.Id);
        }
    }
}