﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace App.Domain.Helpers
{
    public class DbCleaner
    {
        private readonly DbContext _configuration;
        private static readonly string[] _ignoredTables = new[] { "sysdiagrams", "usd_AppliedDatabaseScript" };
        private static string[] _tablesToDelete;
        private static string _deleteSql;
        private static object _lockObj = new object();
        private static bool _initialized;

        public DbCleaner(DbContext sessionSource)
        {
            _configuration = sessionSource;

            BuildDeleteTables();
        }

        private class Relationship
        {
            public string PrimaryKeyTable { get; private set; }

            public string ForeignKeyTable { get; private set; }
        }

        public virtual void DeleteAllData()
        {
            if (string.IsNullOrEmpty(_deleteSql))
            {
                return;
            }

            MakeSureConnectionIsOpen();
            using (var command = _configuration.Database.Connection.CreateCommand())
            {
                command.CommandText = _deleteSql;
                command.ExecuteNonQuery();
            }
        }

        public static string[] GetTables()
        {
            return _tablesToDelete;
        }

        private void MakeSureConnectionIsOpen()
        {
            if (_configuration.Database.Connection.State != ConnectionState.Open)
            {
                _configuration.Database.Connection.Open();
            }
        }

        private void BuildDeleteTables()
        {
            if (!_initialized)
            {
                lock (_lockObj)
                {
                    if (!_initialized)
                    {
                        MakeSureConnectionIsOpen();

                        var allTables = GetAllTables(_configuration);

                        var allRelationships = GetRelationships(_configuration);

                        _tablesToDelete = BuildTableList(allTables, allRelationships);

                        _deleteSql = BuildTableSql(_tablesToDelete);

                        _initialized = true;
                    }
                }
            }
        }

        private static string BuildTableSql(IEnumerable<string> tablesToDelete)
        {
            string completeQuery = "";
            foreach (var tableName in tablesToDelete)
            {
                completeQuery += String.Format("DELETE from [{0}]; IF EXISTS (SELECT 1 FROM sys.identity_columns WHERE OBJECT_NAME(OBJECT_ID) = '{0}' AND last_value IS NOT NULL) DBCC CHECKIDENT ([{0}], RESEED, 0);", tableName);
            }
            return completeQuery;
        }

        private static string[] BuildTableList(ICollection<string> allTables, ICollection<Relationship> allRelationships)
        {
            var tablesToDelete = new List<string>();

            while (allTables.Any())
            {
                var leafTables = allTables.Except(allRelationships.Select(rel => rel.PrimaryKeyTable)).ToArray();

                tablesToDelete.AddRange(leafTables);

                foreach (var leafTable in leafTables)
                {
                    allTables.Remove(leafTable);
                    var relToRemove = allRelationships.Where(rel => rel.ForeignKeyTable == leafTable).ToArray();
                    foreach (var rel in relToRemove)
                    {
                        allRelationships.Remove(rel);
                    }
                }
            }

            return tablesToDelete.ToArray();
        }

        private static IList<Relationship> GetRelationships(DbContext context)
        {
            var query = @"SELECT so_pk.name AS PrimaryKeyTable, so_fk.name AS ForeignKeyTable
                            FROM sysforeignkeys sfk
                      INNER JOIN sysobjects so_pk ON sfk.rkeyid = so_pk.id
                      INNER JOIN sysobjects so_fk ON sfk.fkeyid = so_fk.id
                        ORDER BY so_pk.name, so_fk.name";

            return context.Database.SqlQuery<Relationship>(query).ToList();
        }

        private static IList<string> GetAllTables(DbContext context)
        {
            return context.Database.SqlQuery<string>("SELECT name FROM sys.tables").ToList<string>().Except(_ignoredTables).ToList();
        }
    }
}