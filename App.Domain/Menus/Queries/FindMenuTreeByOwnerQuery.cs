﻿using App.Core.Cqrs;
using App.Domain.Models.Tree;

namespace App.Domain.Menus.Queries
{
    public class FindMenuTreeByOwnerQuery : IQuery
    {
        public ITreeOwner TreeOwner { get; set; }

        public FindMenuTreeByOwnerQuery(ITreeOwner treeOwner)
        {
            TreeOwner = treeOwner;
        }
    }
}
