﻿using App.Core.Cqrs;

namespace App.Domain.Menus.Queries
{
    public class SingleMenuQuery : IQuery
    {
        public int Id { get; set; }

        public SingleMenuQuery(int id)
        {
            Id = id;
        }
    }
}
