﻿using System.Linq;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities;

namespace App.Domain.Rows.Queries
{
    public class SingleRowQueryHandler : IQueryHandler<SingleRowQuery, Row>
    {
        private EFDbContext _context;

        public SingleRowQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public Row Retrieve(SingleRowQuery query)
        {
            return _context.Rows.Include("Values").Include("Values.Field").SingleOrDefault(o => o.Id == query.Id);
        }
    }
}
