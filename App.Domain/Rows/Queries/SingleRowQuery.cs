﻿using App.Core.Cqrs;

namespace App.Domain.Rows.Queries
{
    public class SingleRowQuery : IQuery
    {
        public int Id { get; set; }
    }
}
