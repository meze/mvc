﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Models
{
    public class PagedResult<T>
    {
        public int Total { get; set; }
        public IEnumerable<T> Items { get; set; }

        public PagedResult()
            : this(new List<T>(), 0)
        {
        }

        public PagedResult(IEnumerable<T> result, int total)
        {
            Items = result;
            Total = total;
        }
    }
}
