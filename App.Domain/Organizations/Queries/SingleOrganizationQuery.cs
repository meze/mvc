﻿using App.Core.Cqrs;

namespace App.Domain.Organizations.Queries
{
    public class SingleOrganizationQuery : IQuery
    {
        public int Id { get; set; }
    }
}
