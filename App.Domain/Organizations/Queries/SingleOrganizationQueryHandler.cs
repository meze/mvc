﻿using System.Linq;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;

namespace App.Domain.Organizations.Queries
{
    public class SingleOrganizationQueryHandler : IQueryHandler<SingleOrganizationQuery, Organization>
    {
        private EFDbContext _context;

        public SingleOrganizationQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public Organization Retrieve(SingleOrganizationQuery query)
        {
            return _context.Organizations.SingleOrDefault(o => o.Id == query.Id);
        }
    }
}
