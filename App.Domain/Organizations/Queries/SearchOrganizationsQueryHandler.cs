﻿using System.Linq;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using App.Domain.Models;
using EntityFramework.Extensions;

namespace App.Domain.Organizations.Queries
{
    public class SearchOrganizationsQueryHandler : IQueryHandler<SearchOrganizationsQuery, PagedResult<Organization>>
    {
        private EFDbContext _context;

        public SearchOrganizationsQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public PagedResult<Organization> Retrieve(SearchOrganizationsQuery query)
        {
            IQueryable<Organization> q = _context.Organizations;

            if (query.HasQuery)
            {
                q = q.Where(o => o.Name.Contains(query.Filter)
                    || o.State.Contains(query.Filter)
                    || o.OwnerEmail.Contains(query.Filter)
                    || o.City.Contains(query.Filter));
            }

            q = q.OrderBy(o => o.Id);

            var totalQuery = q.FutureCount();
            var resultQuery = q.Skip(query.SkipNumber).Take(query.PerPage).Future();

            return new PagedResult<Organization>(resultQuery.ToList(), totalQuery.Value);
        }
    }
}
