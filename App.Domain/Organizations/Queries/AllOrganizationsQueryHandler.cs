﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;

namespace App.Domain.Organizations.Queries
{
    public class AllOrganizationsQueryHandler : IQueryHandler<AllOrganizationsQuery, IEnumerable<Organization>>
    {
        private EFDbContext _context;

        public AllOrganizationsQueryHandler(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Organization> Retrieve(AllOrganizationsQuery query)
        {
            return _context.Organizations.Include("Sites").ToList();
        }
    }
}
