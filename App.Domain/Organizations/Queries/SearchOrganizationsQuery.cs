﻿using App.Core.Cqrs;
using App.Domain.Abstract;

namespace App.Domain.Organizations.Queries
{
    public class SearchOrganizationsQuery : SearchQuery, IQuery
    {
    }
}
