﻿using App.Core.Cqrs;
using System.Collections.Generic;

namespace App.Domain.Organizations.Commands
{
    public class RemoveOrganizationsCommand : ICommand
    {
        /// <summary>
        /// A list to store organization's identifiers. In most cases we want to remove only one record.
        /// </summary>
        private IList<int> IdList = new List<int>(1);

        public IList<int> IdsToRemove
        {
            get
            {
                return IdList;
            }
        }
    }
}