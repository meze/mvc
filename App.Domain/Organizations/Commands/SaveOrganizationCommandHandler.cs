﻿using App.Core.Cqrs;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Organizations.Commands
{
    public class SaveOrganizationCommandHandler : ICommandHandler<SaveOrganizationCommand>
    {
        private EFDbContext _context;

        public SaveOrganizationCommandHandler(EFDbContext context)
        {
            _context = context;
        }

        public void Execute(SaveOrganizationCommand command)
        {
            Organization organization;
            bool isNew = false;

            if (command.Id > 0)
            {
                organization = _context.Organizations.SingleOrDefault(o => o.Id == command.Id);
            }
            else
            {
                isNew = true;
                organization = new Organization();
            }

            organization.Name = command.Name;
            organization.OwnerEmail = command.OwnerEmail;
            organization.State = command.State;
            organization.Zip = command.Zip;
            organization.City = command.City;

            if (isNew)
            {
                _context.Organizations.Add(organization);
            }
            _context.SaveChanges();

            command.Id = organization.Id;
        }
    }
}