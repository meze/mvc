﻿using App.Core.Cqrs;

namespace App.Domain.Organizations.Commands
{
    public class SaveOrganizationCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string OwnerEmail { get; set; }
    }
}
