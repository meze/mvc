﻿using App.Core.Cqrs;
using App.Core.Logging;
using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Organizations.Commands
{
    public class RemoveOrganizationsCommandHandler : ICommandHandler<RemoveOrganizationsCommand>
    {
        private EFDbContext _context;
        private ILogger _logger;

        public RemoveOrganizationsCommandHandler(EFDbContext context, ILogger logger)
        {
            _logger = logger;
            _context = context;
        }

        public void Execute(RemoveOrganizationsCommand command)
        {
            var itemsToDelete = _context.Organizations.Where(x => command.IdsToRemove.Contains(x.Id));
            _context.Organizations.RemoveRange(itemsToDelete);
            _context.SaveChanges();

            _logger.Log(EventTypes.OrganizationRemoved, string.Join(", ", command.IdsToRemove));
        }
    }
}