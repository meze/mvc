﻿namespace App.Domain.Entities.Structure
{
    public class SiteOption
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
}
