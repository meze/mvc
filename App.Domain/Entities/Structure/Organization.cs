﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace App.Domain.Entities.Structure
{
    public class Organization
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
        
        public string OwnerEmail { get; set; }

        public virtual ICollection<Site> Sites { get; set; }

        [JsonConstructor]
        public Organization()
        {
            Sites = new List<Site>();
        }

        public Organization(string name)
            : this()
        {
            Name = name;
        }

        public void AddSite(Site site)
        {
            Sites.Add(site);
        }
    }
}
