﻿using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Entities.Structure
{
    public class SiteOptionValue
    {
        public int Id { get; set; }
        public int SiteOptionId { get; set; }
        public string Value { get; set; }

        public SiteOption SiteOption { get; set; }
    }
}
