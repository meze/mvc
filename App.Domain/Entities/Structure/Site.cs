﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Entities.Structure
{
    public class Site
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Domain { get; set; }

        public virtual ICollection<SiteOptionValue> Options { get; set; }

        public int? ConfigId { get; set; }
        [ForeignKey("ConfigId")]
        public Config Config { get; set; }

        public string OwnerEmail { get; set; }
        public string BasePath { get; set; }

        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
    }
}