﻿using App.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace App.Domain.Entities
{
    public class Row
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ModuleId { get; set; }

        public virtual Module Module { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public virtual ICollection<RowValue> Values { get; set; }

        public Row()
        {
            CreatedAt = DateTimeOffset.Now;
            UpdatedAt = CreatedAt;
            Values = new List<RowValue>();
        }

        public void AddValue(Field field, string value)
        {
            Values.Add(new RowValue
            {
                Value = value,
                Field = field,
                Row = this
            });
        }

        public T Get<T>(string name)
        {
            if (Module != null)
            {
                Field field = Module.GetField(name);
                if (field != null)
                {
                    var converterFunc = ((FieldType<T>)field.GetFieldType()).Converter;

                    return converterFunc(Get(field.Name));
                }
            }

            return default(T);
        }

        public string Get(string name)
        {
            var value = Values.FirstOrDefault(v => v.Field.Name == name);

            return value != null ? value.Value : null;
        }

        public IDictionary<string, string> ExportValues()
        {
            return Values.Select(v => new { Name = v.Field.Name, Value = v.Value }).ToDictionary(p => p.Name, p => p.Value);
        }
    }
}