﻿namespace App.Domain.Entities
{
    public class Config
    {
        public int Id { get; set; }
        public string BackgroundColor { get; set; }
    }
}
