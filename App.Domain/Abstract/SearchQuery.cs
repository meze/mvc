﻿using System;

namespace App.Domain.Abstract
{
    abstract public class SearchQuery
    {
        public const int DEFAULT_PER_PAGE = 25;
        private const int LIMIT_PER_PAGE = 100;
        private int _page = 1;
        private int _perPage = DEFAULT_PER_PAGE;

        public bool HasQuery
        {
            get
            {
                return !String.IsNullOrEmpty(Filter);
            }
        }

        public int Page
        {
            get
            {
                return _page;
            }

            set
            {
                if (value > 0)
                {
                    _page = value;
                }
            }
        }

        public int PerPage
        {
            get
            {
                return _perPage;
            }
            set
            {
                if (value > 0)
                {
                    _perPage = Math.Min(Math.Abs(value), LIMIT_PER_PAGE);
                }
            }
        }

        public string Filter
        {
            get;
            set;
        }

        public int SkipNumber
        {
            get
            {
                return PerPage * (Page - 1);
            }
        }
    }
}