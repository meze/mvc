﻿using App.Domain.Entities.Structure;
using App.Domain.Sites.Commands;
using Xunit;

namespace App.IntegrationTests.Organizations.Saves_changes
{
    public class When_site_is_saved : DatabaseTest
    {
        private Organization _organization;

        public override void Seed()
        {
            base.Seed();

            _organization = CreateOrganization("Organization");
        }

        [Fact]
        public void ThenItShouldCreateIfNotExist()
        {
            var handler = new SaveSiteCommandHandler(Context);
            var command = new SaveSiteCommand
            {
                OwnerEmail = "owner@example.com",
                Domain = "example.com",
                Title = "First Site",
                Organization = _organization
            };

            handler.Execute(command);

            var site = GetSite(command.Id);

            Assert.NotNull(site.Id);
            Assert.Equal(command.OwnerEmail, site.OwnerEmail);
            Assert.Equal(command.Domain, site.Domain);
            Assert.Equal(command.Organization, site.Organization);
            Assert.Equal(command.Title, site.Title);
        }

        [Fact]
        public void ThenItShouldUpdateExisting()
        {
            var newSite = CreateSite("test.com", _organization);
            SaveChanges();

            var handler = new SaveSiteCommandHandler(Context);
            var command = new SaveSiteCommand
            {
                Id = newSite.Id,
                OwnerEmail = "updated@example.com",
                Domain = "updated.com",
                Title = "Updated Site",
                Organization = _organization
            };

            handler.Execute(command);

            var site = GetSite(newSite.Id);

            Assert.Equal(command.OwnerEmail, site.OwnerEmail);
            Assert.Equal(command.Domain, site.Domain);
            Assert.Equal(command.Title, site.Title);
        }
    }
}