﻿using App.Domain.Sites.Commands;
using Xunit;

namespace App.IntegrationTests.Sites.Removes
{
    public class When_site_is_removed : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            var organization = CreateOrganization("Acme");

            CreateSite("Needed", organization);
            CreateSite("Unneeded", organization);
        }

        [Fact]
        public void ThenItShouldNotBeInDatabaseAnymore()
        {
            var site = GetSite(2);
            var handler = new RemoveSitesCommandHandler(Context);
            var command = new RemoveSitesCommand();

            command.IdsToRemove.Add(site.Id);

            handler.Execute(command);

            var removedSite = GetSite(site.Id);
            var neededSite = GetSite(1);

            Assert.Null(removedSite);
            Assert.NotNull(neededSite);
        }
    }
}