﻿using App.Domain.Entities.Structure;
using App.Domain.Models.Tree;
using App.Domain.Sites.Queries;
using Moq;
using System.Linq;
using Xunit;

namespace App.IntegrationTests.Organizations.Retrieves_from_storage
{
    public class When_asked_for_all_sites : DatabaseTest
    {
        private Organization _organization;

        public override void Seed()
        {
            base.Seed();

            _organization = CreateOrganization("acme");
            CreateSite("example.com", _organization);
            CreateSite("test.com", _organization);

            var organization = CreateOrganization("another organization");
            CreateSite("another.example.com", organization);
        }

        [Fact]
        public void ThenReturnsAllSitesInOrganization()
        {
            var handler = new SearchSitesQueryHandler(Context);
            var items = handler.Retrieve(new SearchSitesQuery { Organization = _organization });

            Assert.Equal(2, items.Total);
        }
    }
}