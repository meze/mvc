﻿using App.Domain.Models.Tree;
using App.Domain.Organizations.Queries;
using Moq;
using System.Linq;
using Xunit;

namespace App.IntegrationTests.Organizations.Retrieves_from_storage
{
    public class When_asked_for_all_organizations : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            var organization = CreateOrganization("has site");
            CreateSite("example.com", organization);
            CreateOrganization("another one");
        }

        [Fact]
        public void ThenReturnsAllOrganizationsWithTheirSites()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new AllOrganizationsQueryHandler(Context);
            var items = handler.Retrieve(new AllOrganizationsQuery());

            Assert.Equal(2, items.Count());
            Assert.Equal(1, items.Single(x => x.Name == "has site").Sites.Count);
        }
    }
}