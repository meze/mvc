﻿using App.Domain.Models.Tree;
using App.Domain.Organizations.Queries;
using Moq;
using Xunit;

namespace App.IntegrationTests.Organizations.Retrieves_from_storage
{
    public class When_asked_for_an_organization : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            CreateOrganization("first");
            CreateOrganization("second");
        }

        [Fact]
        public void ThenReturnsSingleOrganization()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new SingleOrganizationQueryHandler(Context);
            var single = handler.Retrieve(new SingleOrganizationQuery { Id = 1 });

            Assert.Equal(1, single.Id);
        }
    }
}