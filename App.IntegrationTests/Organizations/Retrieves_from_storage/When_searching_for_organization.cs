﻿using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using App.Domain.Helpers;
using App.Domain.Models.Tree;
using App.Domain.Organizations.Queries;
using Moq;
using System.Linq;
using Xunit;

namespace App.IntegrationTests.Organizations.Retrieves_from_storage
{
    public class When_searching_for_organization : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            var organization = CreateOrganization("has site");
            CreateSite("example.com", organization);
            CreateOrganization("ignore me");
            CreateOrganization("find me");
            CreateOrganization("find me");
        }

        [Fact]
        public void ThenFindsFourRecordsIfKeywordNotGiven()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new SearchOrganizationsQueryHandler(Context);

            var all = handler.Retrieve(new SearchOrganizationsQuery());

            Assert.Equal(4, all.Total);
        }

        [Fact]
        public void ThenFindsOnlyTwoOfFourRecordsIfKeywordFound()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new SearchOrganizationsQueryHandler(Context);

            var foundByQuery = handler.Retrieve(new SearchOrganizationsQuery
            {
                Filter = "find"
            });

            Assert.Equal(2, foundByQuery.Total);
        }

        [Fact]
        public void ThenReturnsNothingIfNoKeywordFound()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new SearchOrganizationsQueryHandler(Context);

            var notFoundByQuery = handler.Retrieve(new SearchOrganizationsQuery
            {
                Filter = "something"
            });

            Assert.Equal(0, notFoundByQuery.Total);
        }
    }
}