﻿using App.Domain.Entities.Structure;
using App.Domain.Organizations.Commands;
using System.Linq;
using Xunit;

namespace App.IntegrationTests.Organizations.Saves_changes
{
    public class When_organization_is_saved : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            CreateOrganization("Organization");
        }

        [Fact]
        public void ThenItShouldCreateOrganizationIfNotExist()
        {
            var handler = new SaveOrganizationCommandHandler(Context);
            var command = new SaveOrganizationCommand
            {
                Name = "ACME",
                City = "Bedford",
                OwnerEmail = "owner@example.com",
                Zip = "10011",
                State = "TX"
            };

            handler.Execute(command);

            var organization = GetOrganization(command.Id);

            Assert.NotNull(command.Id);
            Assert.Equal(command.Name, organization.Name);
            Assert.Equal(command.City, organization.City);
            Assert.Equal(command.OwnerEmail, organization.OwnerEmail);
            Assert.Equal(command.State, organization.State);
            Assert.Equal(command.Zip, organization.Zip);
        }

        [Fact]
        public void ThenItShouldUpdateExistingOrganization()
        {
            var handler = new SaveOrganizationCommandHandler(Context);
            var command = new SaveOrganizationCommand
            {
                Id = 1,
                Name = "Updated Name",
                City = "Bedford"
            };

            handler.Execute(command);

            var organization = GetOrganization(1);

            Assert.Equal(command.Name, organization.Name);
            Assert.Equal(command.City, organization.City);
        }
    }
}