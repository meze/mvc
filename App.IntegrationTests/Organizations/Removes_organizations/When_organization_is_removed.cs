﻿using App.Domain.Entities.Structure;
using App.Domain.Organizations.Commands;
using App.Tests;
using System.Linq;
using Xunit;

namespace App.IntegrationTests.Organizations.Removes_organizations
{
    public class When_organization_is_removed : DatabaseTest
    {
        public override void Seed()
        {
            base.Seed();

            CreateOrganization("Needed organization");
            CreateOrganization("Unneeded organization");
        }

        [Fact]
        public void ThenItShouldNotBeInDatabaseAnymore()
        {
            var organization = GetOrganization(2);
            var handler = new RemoveOrganizationsCommandHandler(Context, Utils.GetLogger());
            var command = new RemoveOrganizationsCommand();

            command.IdsToRemove.Add(organization.Id);

            handler.Execute(command);

            var organizationAfterDeletion = GetOrganization(organization.Id);
            var neededOrganization = GetOrganization(1);

            Assert.Null(organizationAfterDeletion);
            Assert.NotNull(neededOrganization);
        }
    }
}