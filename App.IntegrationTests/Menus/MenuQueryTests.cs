﻿using App.Domain.Concrete;
using App.Domain.Entities.Navigation;
using App.Domain.Helpers;
using App.Domain.Menus.Queries;
using App.Domain.Models.Tree;
using Moq;
using Xunit;

namespace App.IntegrationTests.Menus
{
    public class MenuQueryTests : DatabaseTest
    {
        public override void Seed()
        {
            var menu = new Menu
            {
                Id = 1
            };
            Context.Menus.Add(menu);
            var menu2 = new Menu
            {
                Id = 2
            };
            Context.Menus.Add(menu2);
            Context.MenuItems.Add(new MenuItem
            {
                Id = 2,
                Path = "/1",
                Menu = menu
            });
            Context.MenuItems.Add(new MenuItem
            {
                Id = 1,
                Path = "/",
                Menu = menu
            });
            Context.MenuItems.Add(new MenuItem
            {
                Id = 3,
                Path = "/",
                Menu = menu2
            });
        }

        [Fact]
        public void FindsAndBuildsTrees()
        {
            var treeOwner = new Mock<ITreeOwner>();
            var handler = new FindMenuTreeByOwnerQueryHandler(Context);
            treeOwner.Setup(o => o.GetOwnerId()).Returns(1);

            var tree = handler.Retrieve(new FindMenuTreeByOwnerQuery(treeOwner.Object));

            Assert.Equal(2, tree.Count());
            Assert.Equal("/", tree.GetPath());
        }
    }
}