﻿using App.Domain.Concrete;
using App.Domain.Entities.Structure;
using App.Domain.Helpers;
using App.Domain.Models.Tree;
using App.Domain.Organizations.Queries;
using Moq;
using System;
using System.Linq;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace App.IntegrationTests
{
    abstract public class DatabaseTest
    {
        private EFDbContext _context;

        public EFDbContext Context
        {
            get
            {
                return _context;
            }
        }

        public DatabaseTest()
        {
            var context = new EFDbContext();
            _context = context;
            var deleter = new DbCleaner(context);
            deleter.DeleteAllData();

            Seed();

            SaveChanges();
        }

        public virtual void Seed()
        {
        }

        protected void SaveChanges()
        {
            Context.SaveChanges();
        }

        protected Organization CreateOrganization(string name)
        {
            var organization = new Organization
            {
                Name = name
            };

            Context.Organizations.Add(organization);

            return organization;
        }

        protected Site CreateSite(string domain, Organization organization = null)
        {
            var site = new Site
            {
                Domain = domain,
                Organization = organization
            };
            Context.Sites.Add(site);

            return site;
        }

        protected Organization GetOrganization(int id)
        {
            return Context.Organizations.SingleOrDefault(x => x.Id == id);
        }

        protected Site GetSite(int id)
        {
            return Context.Sites.SingleOrDefault(x => x.Id == id);
        }
    }
}