﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Linq;
using System.Threading;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace App.AdminTests
{
    internal class MainMenuPage : PageBase
    {
        public MainMenuPage(IWebDriver driver)
            : base(driver)
        {
            driver.WaitUntilElementIsPresent(By.XPath("//div[contains(@class, 'main-menu')]//tr[contains(@class, 'x-grid-row')]"));
        }

        public OrganizationsPage GoToOrganizations()
        {
            var orgs = _driver.FindElement(By.XPath("//div[contains(@class, 'main-menu')]//div[contains(@class, 'x-tree-view')]//*[text()='Organizations']"));
            new Actions(_driver).DoubleClick(orgs).Build().Perform();

            return new OrganizationsPage(_driver);
        }

        public bool HasItem(string name)
        {
            return _driver.ElementIsPresent(By.XPath("//div[contains(@class, 'main-menu')]//div[contains(@class, 'x-tree-view')]//*[text()='" + name + "']"));
        }
    }

    internal class OrganizationsPage : PageBase
    {
        public OrganizationsPage(IWebDriver driver)
            : base(driver)
        {
            WaitUntilMaskIsGone();
        }

        public OrganizationsPage GoToAdd()
        {
            var add = _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//div[contains(@class,'x-toolbar')]//span[text()='Add']//ancestor::a"));
            add.Click();

            return this;
        }

        public OrganizationsPage FillOrganizationForm(string[] data)
        {
            var form = _driver.FindElement(By.XPath("//div[contains(@class,'organization-form')]"));
            form.FindElement(By.CssSelector(@"input[name=""zip""]")).ClearAndSendKeys(data[3]);
            form.FindElement(By.CssSelector(@"input[name=""name""]")).ClearAndSendKeys(data[0]);
            form.FindElement(By.CssSelector(@"input[name=""ownerEmail""]")).ClearAndSendKeys(data[4]);
            form.FindElement(By.CssSelector(@"input[name=""city""]")).ClearAndSendKeys(data[2]);
            var lastElement = form.FindElement(By.CssSelector(@"input[name=""state""]"));
            lastElement.ClearAndSendKeys(data[1]);
            _driver.Blur(lastElement);

            return this;
        }

        public bool IsAddingFormDisplayed()
        {
            return _driver.FindElement(By.XPath("//div[contains(@class,'organization-add')]")).Displayed;
        }

        public string[] GetRowFor(string organizationName)
        {
            var gridXPath = "//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']";
            if (!_driver.ElementIsVisible(By.XPath(gridXPath)))
            {
                return null;
            }

            var columns = _driver.FindElements(By.XPath(gridXPath + "//ancestor::tr[1]//td[position()>1 and position()<last()]/div"));
            return columns.Select(x => x.Text).ToArray();
        }

        public bool IsErrorToastDisplayed()
        {
            var toast = _driver.FindElement(By.XPath("//div[contains(@class, 'form-error-toast')]"));

            return toast.Displayed;
        }

        public OrganizationsPage SubmitForm()
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'organization-form')]//ancestor::div[contains(@class, 'x-window')]//a[contains(@class, 'save-btn')]")).Click();
            WaitUntilMaskIsGone();

            return this;
        }

        public OrganizationsPage AttemptToDelete()
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//div[contains(@class,'x-toolbar')]//span[text()='Delete']//ancestor::a")).Click();
            _driver.WaitUntilElementIsPresent(By.CssSelector(".remove-grid-item-confirm"));

            return this;
        }

        public OrganizationsPage AttemptToDelete(string organizationName)
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']//ancestor::tr[1]//img[contains(@class, 'x-icon-delete')]")).Click();
            _driver.WaitUntilElementIsPresent(By.CssSelector(".remove-grid-item-confirm"));

            return this;
        }

        public OrganizationsPage Change(string organizationName)
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']//ancestor::tr[1]//img[contains(@class, 'x-icon-edit')]")).Click();
            _driver.WaitUntilElementIsPresent(By.CssSelector(".organization-form"));
            _driver.WaitUntilElementsAreGone(By.CssSelector(".organization-form .x-mask"));

            return this;
        }

        public void ConfirmDeletion()
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'remove-grid-item-confirm')]//span[text()='Yes']//ancestor::a[1]")).Click();
            WaitUntilMaskIsGone();
        }

        public void CancelDeletion()
        {
            _driver.FindElement(By.XPath("//div[contains(@class,'remove-grid-item-confirm')]//span[text()='No']//ancestor::a[1]")).Click();
            WaitUntilMaskIsGone();
        }

        public ContextMenuPage OpenContextMenuFor(string organizationName)
        {
            var action = new Actions(_driver);
            action.MoveToElement(_driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']//ancestor::td[1]")));
            action.ContextClick();
            action.Perform();

            return new ContextMenuPage(_driver.FindElement(By.ClassName("grid-context-menu")));
        }

        public void Choose(string organizationName)
        {
            var checker = _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']//ancestor::tr[1]//div[contains(@class, 'x-grid-row-checker')]"));
            new Actions(_driver).KeyDown(Keys.Shift).Click(checker).KeyUp(Keys.Shift).Perform();
        }

        public int CountRows()
        {
            return _driver.FindElements(By.CssSelector(".organization-grid .x-grid-item")).Count;
        }

        public void SearchBy(string criteria)
        {
            var searchInput = _driver.FindElement(By.CssSelector(".organization-grid .grid-basic-search .x-form-field"));

            searchInput.SendKeys(criteria);
            searchInput.SendKeys(Keys.Enter);

            WaitUntilMaskIsGone();
        }

        public void ResetSearch()
        {
            _driver.FindElement(By.CssSelector(".organization-grid .grid-basic-search .x-form-clear-trigger")).Click();

            WaitUntilMaskIsGone();
        }

        public bool IsSelected(string organizationName)
        {
            var elm = _driver.FindElement(By.XPath("//div[contains(@class,'organization-grid')]//td/div[text()='" + organizationName + "']//ancestor::table[1]"));
            return elm.GetAttribute("class").Split(' ').Contains("x-grid-item-selected");
        }
    }

    internal class ContextMenuPage
    {
        private IWebElement _contextMenu;

        public ContextMenuPage(IWebElement contextMenu)
        {
            _contextMenu = contextMenu;
        }

        public string[] GetActionsNames()
        {
            return _contextMenu.FindElements(By.ClassName("x-menu-item-text")).Select(x => x.Text).ToArray();
        }
    }

    [Collection("Web")]
    public class AdminWebTest : WebTest
    {
        private IWebDriver driver;

        public AdminWebTest(SeleniumTest fixture)
        {
            driver = fixture.GetDriver();
            driver.Navigate().GoToUrl(GetAbsoluteUrl("Admin"));
        }

        /*
        [ClassInitialize]
        public static new void Startup(TestContext context)
        {
            SeleniumTest.Startup(context);
        }

        [ClassCleanup]
        public static new void CleanUp()
        {
            SeleniumTest.CleanUp();
        }*/

        [Fact]
        public void CreatesAnOrganization()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();

            var rowToCreate = new string[] { "Test company", "NY", "New York", "42356", "test@example.com" };

            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            var row = organizations.GetRowFor(rowToCreate[0]);

            Assert.Equal(rowToCreate, row);
            Assert.Equal(1, organizations.CountRows());

            Assert.True(menu.HasItem(rowToCreate[0]));
        }

        [Fact]
        public void ChangesAnOrganization()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "Test company", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            var updatedRow = new string[] { "Test company (updated)", "UP", "New York (updated)", "11111", "updated@example.com" };

            organizations.Change(rowToCreate[0]).FillOrganizationForm(updatedRow).SubmitForm();

            var row = organizations.GetRowFor(updatedRow[0]);

            Assert.Equal(0, rowToCreate.Intersect(updatedRow).Count());
            Assert.Equal(updatedRow, row);
            Assert.False(menu.HasItem(rowToCreate[0]));
            Assert.True(menu.HasItem(updatedRow[0]));
        }

        [Fact]
        public void ShowsErrorsIfFormIsInvalid()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            Assert.True(organizations.IsErrorToastDisplayed());
            Assert.True(organizations.IsAddingFormDisplayed());
        }

        [Fact]
        public void RemovesAnOrganization()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "To remove", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            organizations.AttemptToDelete(rowToCreate[0]).ConfirmDeletion();

            var row = organizations.GetRowFor(rowToCreate[0]);

            Assert.Null(row);
            Assert.False(menu.HasItem(rowToCreate[0]));
        }

        [Fact]
        public void DoesNotRemoveAnOrganizationIfCanceled()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "To not remove", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            organizations.AttemptToDelete(rowToCreate[0]).CancelDeletion();

            var row = organizations.GetRowFor(rowToCreate[0]);

            Assert.True(organizations.IsSelected(rowToCreate[0]));
            Assert.NotNull(row);
        }

        [Fact]
        public void HasContextMenu()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "Test", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            var actionsNames = organizations.OpenContextMenuFor("Test").GetActionsNames();

            Assert.Equal(3, actionsNames.Length);
            Assert.True(organizations.IsSelected("Test"));
        }

        [Fact]
        public void RemovesAFewOrganizations()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var firstRowToCreate = new string[] { "To remove 1", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(firstRowToCreate)
                .SubmitForm();

            var secondRowToCreate = new string[] { "To remove 2", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(secondRowToCreate)
                .SubmitForm();

            organizations.Choose(firstRowToCreate[0]);
            organizations.Choose(secondRowToCreate[0]);
            organizations.AttemptToDelete().ConfirmDeletion();

            var firstRow = organizations.GetRowFor(firstRowToCreate[0]);
            var secondRow = organizations.GetRowFor(secondRowToCreate[0]);

            Assert.Null(firstRow);
            Assert.Null(secondRow);
        }

        [Fact]
        public void RemovesOnlyOneOrganizationIfClickedOnRow()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var firstRowToCreate = new string[] { "To remove 1", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(firstRowToCreate)
                .SubmitForm();

            var secondRowToCreate = new string[] { "To remove 2", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(secondRowToCreate)
                .SubmitForm();

            organizations.Choose(firstRowToCreate[0]);
            organizations.Choose(secondRowToCreate[0]);

            organizations.AttemptToDelete(firstRowToCreate[0]).ConfirmDeletion();

            var firstRow = organizations.GetRowFor(firstRowToCreate[0]);
            var secondRow = organizations.GetRowFor(secondRowToCreate[0]);

            Assert.Null(firstRow);
            Assert.NotNull(secondRow);
        }

        [Fact]
        public void HasNoRows()
        {
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            Assert.Equal(0, organizations.CountRows());
        }

        [Fact]
        public void SearchByCriteria()
        {
            var criteria = "@criteria@";
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var firstRowToCreate = new string[] { "Row 1" + criteria, "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(firstRowToCreate)
                .SubmitForm();

            var secondRowToCreate = new string[] { "Row 2", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(secondRowToCreate)
                .SubmitForm();

            organizations.SearchBy(criteria);

            Assert.Equal(1, organizations.CountRows());
        }

        [Fact]
        public void ResetsSearch()
        {
            var criteria = "@criteria@";
            var menu = new MainMenuPage(driver);
            var organizations = menu.GoToOrganizations();
            var rowToCreate = new string[] { "Row", "NY", "New York", "42356", "test@example.com" };
            organizations
                .GoToAdd()
                .FillOrganizationForm(rowToCreate)
                .SubmitForm();

            organizations.SearchBy(criteria);
            organizations.ResetSearch();

            Assert.Equal(1, organizations.CountRows());
        }
    }
}