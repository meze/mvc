﻿using OpenQA.Selenium;

namespace App.AdminTests
{
    internal class PageBase
    {
        protected IWebDriver _driver;

        public PageBase(IWebDriver driver)
        {
            _driver = driver;
        }

        public void WaitUntilMaskIsGone()
        {
            _driver.WaitUntilElementsAreGone(By.CssSelector(".x-mask"));
        }
    }
}
