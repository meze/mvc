﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;

namespace App.AdminTests
{
    internal static class WebDriverExtension
    {
        private const int DEFAULT_TIMEOUT = 100;
        private const int DEFAULT_IMPLICIT_TIMEOUT = 1;

        public static bool ElementIsVisible(this IWebDriver driver, By by)
        {
            return driver.FindElements(by).Count<IWebElement>(elm =>
            {
                if (elm == null)
                {
                    return false;
                }
                try
                {
                    return elm.Displayed;
                }
                catch
                {
                    return false;
                }
            }) > 0;
        }

        public static bool ElementIsPresent(this IWebDriver driver, By by)
        {
            return driver.FindElements(by).Count > 0;
        }

        public static bool WaitUntilElementIsPresent(this IWebDriver driver, By by, int timeout = DEFAULT_TIMEOUT)
        {
            return driver.WaitUntil(d => d.ElementIsVisible(by), timeout);
        }

        public static bool WaitUntilElementsAreGone(this IWebDriver driver, By by, int timeout = DEFAULT_TIMEOUT)
        {
            return driver.WaitUntil(d => !d.ElementIsVisible(by), timeout);
        }

        public static TResult WaitUntil<TResult>(this IWebDriver driver, Func<IWebDriver, TResult> until, int timeout = DEFAULT_TIMEOUT)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(until);
        }

        public static void EnableImplicitlyWait(this IWebDriver driver)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(DEFAULT_IMPLICIT_TIMEOUT));
        }

        public static void DisableImplicitlyWait(this IWebDriver driver)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));
        }

        public static void ClearAndSendKeys(this IWebElement element, string value)
        {
            element.Clear();
            element.SendKeys(value);
        }

        public static IJavaScriptExecutor Script(this IWebDriver driver)
        {
            return (IJavaScriptExecutor)driver;
        }

        public static void Blur(this IWebDriver driver, IWebElement element)
        {
            driver.Script().ExecuteScript("$('" + element.GetAttribute("id") + "').blur();");
        }
    }
}