﻿using App.AdminTests.IIS;
using System;

namespace App.AdminTests
{
    public abstract class WebTest : IDisposable
    {
        private const int IIS_PORT = 2020;
        private const string DOMAIN = "localhost";
        private string _applicationName;

        private static IISTestRunnerAction action;

        protected WebTest(string applicationName = "App")
        {
            _applicationName = applicationName;
            StartIIS();
        }

        public void Dispose()
        {
            StopIIS();
        }

        public string GetAbsoluteUrl(string relativeUrl)
        {
            if (!relativeUrl.StartsWith("/"))
            {
                relativeUrl = "/" + relativeUrl;
            }

            return String.Format("http://{0}:{1}{2}", DOMAIN, IIS_PORT, relativeUrl);
        }

        private static void StartIIS()
        {
            var config = new IISExpressConfigBuilder();

            config.ApplyWebConfigTransformForConfig("Test");
            config.With(Project.Named("App.WebUI"));
            config.UsePort(IIS_PORT);

            action = config.GetAction();
            action.Startup();
        }

        private static void StopIIS()
        {
            action.Shutdown();
            action = null;
        }
    }
}