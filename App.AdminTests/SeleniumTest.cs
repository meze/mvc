﻿using App.AdminTests.IIS;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace App.AdminTests
{
    public class SeleniumTest : IDisposable
    {
        protected static IWebDriver driver;

        public SeleniumTest()
        {
            StartSelenium();
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }

        public void Dispose()
        {
            StopSelenium();
            driver.Dispose();
            driver = null;
        }

        private static void StartSelenium()
        {
            driver = new ChromeDriver();

            driver.EnableImplicitlyWait();
        }

        private static void StopSelenium()
        {
            driver.Quit();
        }

        /*
        private static void StartIIS()
        {
            var config = new IISExpressConfigBuilder();

            config.ApplyWebConfigTransformForConfig("Test");
            config.With(Project.Named("App.WebUI"));
            config.UsePort(IIS_PORT);

            action = config.GetAction();
            action.Startup();
        }

        private static void StopIIS()
        {
            action.Shutdown();
            action = null;
        }

        public string GetAbsoluteUrl(string relativeUrl)
        {
            if (!relativeUrl.StartsWith("/"))
            {
                relativeUrl = "/" + relativeUrl;
            }

            return String.Format("http://{0}:{1}{2}", DOMAIN, IIS_PORT, relativeUrl);
        }*/
    }
}