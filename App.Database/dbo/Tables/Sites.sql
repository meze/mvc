﻿CREATE TABLE [dbo].[Sites] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Title]          NVARCHAR (MAX) NULL,
    [Domain]         NVARCHAR (MAX) NULL,
    [Zone]           NVARCHAR (MAX) NULL,
    [ConfigId]       INT            NULL,
    [OwnerEmail]     NVARCHAR (MAX) NULL,
    [BasePath]       NVARCHAR (MAX) NULL,
    [OrganizationId] INT            NOT NULL,
    CONSTRAINT [PK_dbo.Sites] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Sites_dbo.Configs_ConfigId] FOREIGN KEY ([ConfigId]) REFERENCES [dbo].[Configs] ([Id]),
    CONSTRAINT [FK_dbo.Sites_dbo.Organizations_OrganizationId] FOREIGN KEY ([OrganizationId]) REFERENCES [dbo].[Organizations] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_OrganizationId]
    ON [dbo].[Sites]([OrganizationId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ConfigId]
    ON [dbo].[Sites]([ConfigId] ASC);

