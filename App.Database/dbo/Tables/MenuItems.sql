﻿CREATE TABLE [dbo].[MenuItems] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Path]             NVARCHAR (MAX) NULL,
    [Link]             NVARCHAR (MAX) NULL,
    [Type]             INT            NOT NULL,
    [DisplayOnDesktop] BIT            NOT NULL,
    [DisplayOnMobile]  BIT            NOT NULL,
    [Sequence]         INT            NOT NULL,
    [Menu_Id]          INT            NULL,
    CONSTRAINT [PK_dbo.MenuItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.MenuItems_dbo.Menus_Menu_Id] FOREIGN KEY ([Menu_Id]) REFERENCES [dbo].[Menus] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Menu_Id]
    ON [dbo].[MenuItems]([Menu_Id] ASC);

