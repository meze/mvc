﻿CREATE TABLE [dbo].[Modules] (
    [Id]     INT            IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (MAX) NULL,
    [SiteId] INT            NOT NULL,
    CONSTRAINT [PK_dbo.Modules] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Modules_dbo.Sites_SiteId] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Sites] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_SiteId]
    ON [dbo].[Modules]([SiteId] ASC);

