﻿CREATE TABLE [dbo].[Rows] (
    [Id]        INT                IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (MAX)     NULL,
    [ModuleId]  INT                NOT NULL,
    [CreatedAt] DATETIMEOFFSET (7) NOT NULL,
    [UpdatedAt] DATETIMEOFFSET (7) NOT NULL,
    CONSTRAINT [PK_dbo.Rows] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Rows_dbo.Modules_ModuleId] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Modules] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ModuleId]
    ON [dbo].[Rows]([ModuleId] ASC);

