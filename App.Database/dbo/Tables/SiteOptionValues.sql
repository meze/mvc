﻿CREATE TABLE [dbo].[SiteOptionValues] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [SiteOptionId] INT            NOT NULL,
    [Value]        NVARCHAR (MAX) NULL,
    [Site_Id]      INT            NULL,
    CONSTRAINT [PK_dbo.SiteOptionValues] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.SiteOptionValues_dbo.SiteOptions_SiteOptionId] FOREIGN KEY ([SiteOptionId]) REFERENCES [dbo].[SiteOptions] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.SiteOptionValues_dbo.Sites_Site_Id] FOREIGN KEY ([Site_Id]) REFERENCES [dbo].[Sites] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Site_Id]
    ON [dbo].[SiteOptionValues]([Site_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SiteOptionId]
    ON [dbo].[SiteOptionValues]([SiteOptionId] ASC);

