﻿CREATE TABLE [dbo].[SiteOptions] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [DefaultValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.SiteOptions] PRIMARY KEY CLUSTERED ([Id] ASC)
);

