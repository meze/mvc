﻿CREATE TABLE [dbo].[RowValues] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [RowId]   INT            NULL,
    [FieldId] INT            NULL,
    [Value]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.RowValues] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.RowValues_dbo.Fields_FieldId] FOREIGN KEY ([FieldId]) REFERENCES [dbo].[Fields] ([Id]),
    CONSTRAINT [FK_dbo.RowValues_dbo.Rows_RowId] FOREIGN KEY ([RowId]) REFERENCES [dbo].[Rows] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FieldId]
    ON [dbo].[RowValues]([FieldId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RowId]
    ON [dbo].[RowValues]([RowId] ASC);

