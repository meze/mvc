﻿CREATE TABLE [dbo].[Menus] (
    [Id]   INT IDENTITY (1, 1) NOT NULL,
    [Type] INT NOT NULL,
    CONSTRAINT [PK_dbo.Menus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

