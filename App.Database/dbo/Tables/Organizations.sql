﻿CREATE TABLE [dbo].[Organizations] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (MAX) NULL,
    [City]       NVARCHAR (MAX) NULL,
    [State]      NVARCHAR (MAX) NULL,
    [Zip]        NVARCHAR (MAX) NULL,
    [OwnerEmail] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Organizations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

