﻿CREATE TABLE [dbo].[Fields] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Label]    NVARCHAR (MAX) NULL,
    [Type]     NVARCHAR (MAX) NULL,
    [Name]     NVARCHAR (MAX) NULL,
    [ModuleId] INT            NOT NULL,
    CONSTRAINT [PK_dbo.Fields] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Fields_dbo.Modules_ModuleId] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Modules] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ModuleId]
    ON [dbo].[Fields]([ModuleId] ASC);

