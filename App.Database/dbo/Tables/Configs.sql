﻿CREATE TABLE [dbo].[Configs] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [BackgroundColor] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Configs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

