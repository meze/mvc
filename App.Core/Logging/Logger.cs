﻿using System;

namespace App.Core.Logging
{
    public class Logger : ILogger
    {
        private NLog.Logger _logger;
        private LoggerConfiguration _configuration;

        public Logger(LoggerConfiguration configuration, NLog.Logger logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public void Log(EventTypes type, string message)
        {
            var logMessage = Enum.GetName(typeof(EventTypes), type) + ": " + message;

            _logger.Info(logMessage);
            //throw new NotImplementedException();
        }

        public void Log(EventTypes eventTypes, string message, params object[] args)
        {
            throw new NotImplementedException();
        }
    }
}