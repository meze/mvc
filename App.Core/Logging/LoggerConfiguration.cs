﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace App.Core.Logging
{
    public class LoggerConfiguration
    {
        private XElement _config;

        public LoggerConfiguration(string configPath)
        {
            if (!File.Exists(configPath))
            {
                throw new ArgumentException("File not found", "configPath");
            }
            _config = XElement.Load(configPath);
        }

        public string GetSeverity(string eventType)
        {
            var element = (from el in _config.Elements("Event")
                           where (string)el.Attribute("Type") == eventType
                           select el).FirstOrDefault();

            if (element != null)
            {
                return (string)element.Attribute("Severity");
            }

            return null;
        }
    }
}