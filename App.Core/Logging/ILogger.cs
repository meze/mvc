﻿namespace App.Core.Logging
{
    public interface ILogger
    {
        void Log(EventTypes type, string message);

        void Log(EventTypes eventTypes, string message, params object[] args);
    }
}