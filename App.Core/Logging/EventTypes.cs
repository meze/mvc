﻿namespace App.Core.Logging
{
    public enum EventTypes
    {
        Unspecified = 0,

        // success 1xxx-2xxx

        // Organizations: 11xx
        OrganizationAdded = 1100,

        OrganizationRemoved = 1101

        // failure 3xxx

        // fatal 4xxx
    }
}