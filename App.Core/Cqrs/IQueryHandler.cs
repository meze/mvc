﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Cqrs
{
    public interface IQueryHandler<in TParameter, out TResult>
        where TParameter : IQuery
    {
        TResult Retrieve(TParameter query);
    }
}
