﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs
{
    public class Reject<T>
    {
        public string Message { get; set; }

        public T Reason { get; set; }
    }
}