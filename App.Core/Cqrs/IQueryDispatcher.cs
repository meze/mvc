﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs
{
    public interface IQueryDispatcher
    {   
        TResult Dispatch<TParameter, TResult>(TParameter query)
            where TParameter : IQuery;
    }
}