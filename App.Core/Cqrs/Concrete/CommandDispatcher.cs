﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs.Concrete
{
    public class CommandDispatcher : ICommandDispatcher
    {
        IHandlerFactory _factory;

        public CommandDispatcher(IHandlerFactory factory)
        {
            _factory = factory;
        }

        public void Dispatch<TParameter>(TParameter command) where TParameter : ICommand
        {
            var handler = _factory.Get<ICommandHandler<TParameter>>();
            handler.Execute(command);
        }
    }
}