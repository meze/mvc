﻿using App.Core.Logging;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs.Concrete
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IHandlerFactory _factory;

        private ILogger _logger;

        public QueryDispatcher(IHandlerFactory factory, ILogger logger)
        {
            _logger = logger;
            _factory = factory;
        }

        public TResult Dispatch<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
        {
            _logger.Log(EventTypes.OrganizationAdded, "Executing a query of type {0}", typeof(TParameter).FullName);
            var handler = _factory.Get<IQueryHandler<TParameter, TResult>>();
            var result = handler.Retrieve(query);
            _logger.Log(EventTypes.OrganizationAdded, "Finished a query of type {0}", typeof(TParameter).FullName);

            return result;
        }
    }
}