﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs
{
    public interface ICommandHandler<in TParameter> where TParameter : ICommand
    {
        void Execute(TParameter command);
    }
}