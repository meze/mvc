﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.Cqrs
{
    public interface ICommandDispatcher
    {
        void Dispatch<TParameter>(TParameter command) where TParameter : ICommand;
    }
}