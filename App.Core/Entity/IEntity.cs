﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entity
{
    public interface IDomainEvent
    {
    }

    public interface IEntity
    {
        ICollection<IDomainEvent> Events { get; }
    }
}